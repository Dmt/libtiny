// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#include <tiny/interface/loop.h>
#include <tiny/log.h>
#include <tiny/interface/context.h>

#include <liburing.h>
#include <unistd.h>

#include <sys/signalfd.h>
#include <sys/timerfd.h>

// Implementation of loop using io_uring

struct tiny_loop {
	struct tiny_dispatch d;
	struct tiny_logger *log;
	struct io_uring ring;
	int state; // the running state of the loop
	struct tiny_list_node op_pool, pending_ops, idle_ops;
};

static int loop_submit(struct tiny_loop *l)
{
	int res = io_uring_submit(&l->ring);
	if (res < 0) {
		tiny_log_error(l->log, "%p: io_uring_submit failed with %d", res);
		return res;
	}
	return 0;
}

static int loop_get_sqe(struct tiny_loop *l, struct io_uring_sqe **sqe)
{
	struct io_uring *r = &l->ring;
	*sqe = io_uring_get_sqe(r);
	if (!*sqe) {
		int res = loop_submit(l);
		if (res < 0)
			return res;
		*sqe = io_uring_get_sqe(r);
	}
	return *sqe ? 0 : -ENOMEM;
}

typedef void(tiny_loop_op_prep_t)(struct tiny_loop_op*, struct io_uring_sqe*);
typedef int(tiny_loop_op_cb_t)(struct tiny_loop_op*, struct io_uring_cqe*);
// internal function table
struct op_ifuncs {
	tiny_loop_op_prep_t *prep; // call the prep for the given op
	tiny_loop_op_cb_t *complete; // the io_uring completion callback
	tiny_loop_cb_t *cb; // the optional user defined callback
	tiny_free_t *cleanup;
};

struct tiny_loop_op {
	struct tiny_dispatch d;
	struct tiny_list_node node;
	struct op_ifuncs f;
	struct tiny_loop *loop;
	struct tiny_clean_ptr ptr;
	struct tiny_fd fd;
	union {
		struct tiny_buffer buf;
		struct signalfd_siginfo siginfo;
		uint64_t u64;
	};
	uint16_t refcount;
	bool armed : 1;
};

static void op_free(struct tiny_loop_op *op)
{
	struct tiny_loop *loop = op->loop;
	tiny_clean_ptr_cleanup(&op->ptr);
	tiny_fd_cleanup(&op->fd);
	if (op->f.cleanup)
		op->f.cleanup(op);
	tiny_list_append(&loop->op_pool, &op->node);
}

static bool op_check_free(struct tiny_loop_op *op)
{
	if (!op->armed && !op->refcount) {
		op_free(op);
		return true;
	}
	return false;
}

static int impl_op_arm(struct tiny_loop_op *op)
{
	tiny_assert_exec(op->f.prep, return -ENOSYS);
	if (op->loop->state != true)
		return -ESHUTDOWN;
	if (op->armed)
		return -EALREADY;
	struct io_uring_sqe *sqe;
	int res = loop_get_sqe(op->loop, &sqe);
	if (res)
		return res;
	// now that we have an sqe, prep it
	op->f.prep(op, sqe);
	// set the op as the user data
	io_uring_sqe_set_data(sqe, op);
	//the operation is now armed
	op->armed = true;
	tiny_list_append(&op->loop->pending_ops, &op->node);
	// submit can fail for a bunch of reasons. At this point we technically
	// don't care. From the moment we got an entry in the submission queue,
	// the event is ready to be handed off to the kernel. If it turns out this
	// can fail in ways that are really bad, we can return the error from here
	// and adjust the 'armed' flag
	loop_submit(op->loop);
	return 0;
}

static int impl_op_cancel(struct tiny_loop_op *op)
{
	struct io_uring_sqe *sqe = io_uring_get_sqe(&op->loop->ring);
	tiny_assert_exec(sqe, return -ENOMEM);
	// We don't currently care about the cancelation event. So
	// we do not attach user data to it
	io_uring_prep_cancel(sqe, op, 0);
	io_uring_submit(&op->loop->ring);
	return 0;
}

static int impl_op_user_data(struct tiny_loop_op *op, void **data)
{
	tiny_assert_exec(data, return -EINVAL);
	*data = op->ptr.ptr;
	return 0;
}

static int impl_op_set_user_data(struct tiny_loop_op *op,
								 struct tiny_clean_ptr ptr)
{
	tiny_clean_ptr_cleanup(&op->ptr);
	op->ptr = ptr;
	return 0;
}

static int impl_op_buffer(struct tiny_loop_op *op, struct tiny_buffer **buf)
{
	tiny_assert_exec(buf, return -EINVAL);
	*buf = &op->buf;
	return 0;
}

static int impl_op_unref(struct tiny_loop_op *op)
{
	if (!op->refcount) {
		tiny_log_warning(op->loop->log,
						 "%p: %p already at zero refcount. Is there a bug?",
						 op->loop, op);
		return -EBADE;
	}
	op->refcount--;
	op_check_free(op);
	return 0;
}

static struct tiny_loop_op *op_new(struct tiny_loop *loop,
	struct op_ifuncs funcs, struct tiny_fd fd,
	struct tiny_clean_ptr data, struct tiny_buffer *buf)
{
	struct tiny_loop_op *op =
	    tiny_list_pop_next(&loop->op_pool, struct tiny_loop_op, node);
	if (!op)
		op = tiny_alloc(struct tiny_loop_op);
	if (!op) {
		tiny_clean_ptr_cleanup(&op->ptr);
		tiny_fd_cleanup(&fd);
		return NULL;
	}
	*op = (struct tiny_loop_op) {
		TINY_DISPATCH(),
		TINY_LIST(op->node),
		funcs,
		loop,
		data,
		fd,
		{},
		1,
		false,
	};

	if (buf) {
		static struct tiny_loop_op_functions disp = {
			.arm = impl_op_arm,
			.cancel = impl_op_cancel,
			.buffer = impl_op_buffer,
			.user_data = impl_op_user_data,
			.set_user_data = impl_op_set_user_data,
			.unref = impl_op_unref,
		};
		op->d = TINY_DISPATCH(&disp);
		op->buf = TINY_SWAP(*buf, TINY_BUFFER_INIT);
	}
	else {
		static struct tiny_loop_op_functions disp = {
			.arm = impl_op_arm,
			.cancel = impl_op_cancel,
			.buffer = NULL,
			.user_data = impl_op_user_data,
			.set_user_data = impl_op_set_user_data,
			.unref = impl_op_unref,
		};
		op->d = TINY_DISPATCH(&disp);
	}

	tiny_list_append(&loop->idle_ops, &op->node);
	return op;
}

//////////////// function hooks
static int uring_stop(struct tiny_loop *loop)
{
	// if there has been an error set, don't overwrite it
	if (loop->state > 0)
		loop->state = false;
	// trigger cancellations
	struct tiny_loop_op *op;
	tiny_list_foreach(op, &loop->pending_ops, node)
		impl_op_cancel(op);
	return 0;
}

static int uring_run(struct tiny_loop *loop)
{
	struct io_uring_cqe *cqe;

	loop->state = true;
	while (loop->state > 0 || tiny_list_has_items(&loop->pending_ops)) {
		int ret = io_uring_wait_cqe(&loop->ring, &cqe);
		if (ret < 0)
			return ret;
		struct tiny_loop_op *op = io_uring_cqe_get_data(cqe);
		tiny_log_debug(loop->log, "%p: Processing op %p r:%d", loop, op,
			       cqe->res);
		if (op) {
			// lower the armed flag so the completion has a chance to rearm
			op->armed = false;
			// If the callback returns false, we drop the event.
			int res = op->f.complete(op, cqe);
			if (res < 0 && loop->state == true) {
				tiny_log_error(loop->log,
					"%p: Got error %d from completion, shutting down",
							   loop, res);
				loop->state = res;
				uring_stop(loop);
			}
			// check if the operation needs freeing
			// if it doesn't need freeing but is not armed, move to the idle list
			if (!op_check_free(op) && !op->armed)
				tiny_list_append(&loop->idle_ops, &op->node);
		}
		io_uring_cqe_seen(&loop->ring, cqe);
	}
	return loop->state;
}

// Read/Write ops

static void rw_cleanup(void *ptr)
{
	struct tiny_loop_op *op = ptr;
	op->buf = tiny_buffer_cleanup(&op->buf);
}

static int read_complete(struct tiny_loop_op *op, struct io_uring_cqe *cqe)
{
	// truncate the buffer to the amount of data read
	if (cqe->res >= 0)
		tiny_buffer_truncate(&op->buf, cqe->res);
	if (op->f.cb && op->f.cb(op, cqe->res))
		return tiny_buffer_len(&op->buf);
	return 0;
}

static void read_prep(struct tiny_loop_op *op, struct io_uring_sqe *sqe)
{
	io_uring_prep_read(sqe, op->fd.fd, tiny_buffer_start(&op->buf),
			   tiny_buffer_len(&op->buf), 0);
}

static int uring_read(struct tiny_loop *loop, struct tiny_fd fd,
				       struct tiny_buffer *buf, tiny_loop_cb_t cb,
				       struct tiny_clean_ptr data, struct tiny_loop_op **ret)
{
	tiny_assert_exec(ret, return -EINVAL);
	struct tiny_loop_op *op = op_new(loop, (struct op_ifuncs)
		{read_prep, read_complete, cb, rw_cleanup}, fd, data, buf);
	if (!op)
		return -ENOMEM;
	*ret = op;
	return 0;
}

static int write_complete(struct tiny_loop_op *op, struct io_uring_cqe *cqe)
{
	// trim the front of the buffer by the amount of data written
	if (cqe->res > 0)
		tiny_buffer_trim_start(&op->buf, cqe->res);
	if (op->f.cb && op->f.cb(op, cqe->res))
		return tiny_buffer_len(&op->buf);
	return 0;
}

static void write_prep(struct tiny_loop_op *op, struct io_uring_sqe *sqe)
{
	io_uring_prep_write(sqe, op->fd.fd, tiny_buffer_start(&op->buf),
			    tiny_buffer_len(&op->buf), 0);
}

static int uring_write(struct tiny_loop *loop, struct tiny_fd fd,
					   struct tiny_buffer *buf, tiny_loop_cb_t cb,
					   struct tiny_clean_ptr data, struct tiny_loop_op **ret)
{
	tiny_assert_exec(ret, return -EINVAL);
	struct tiny_loop_op *op = op_new(loop, (struct op_ifuncs)
		{write_prep, write_complete, cb, rw_cleanup}, fd, data, buf);
	if (!op)
		return -ENOMEM;
	*ret = op;
	return 0;
}

// timerfd handling

static int timer_complete(struct tiny_loop_op *op, struct io_uring_cqe *cqe)
{
	if (cqe->res == sizeof(uint64_t)) {
		return op->f.cb(op, op->u64);
	} else if (cqe->res >= 0) {
		return op->f.cb(op, -EINVAL);
	}
	return op->f.cb(op, cqe->res);
}

static void timer_prep(struct tiny_loop_op *op, struct io_uring_sqe *sqe)
{
	io_uring_prep_read(sqe, op->fd.fd, &op->u64, sizeof(op->u64), 0);
}

static struct timespec to_timespec(tiny_nsec_t nsec)
{
	return (struct timespec){nsec / TINY_SECOND, nsec % TINY_SECOND};
}

static int uring_timer(struct tiny_loop *loop, struct tiny_timestamp timeout,
					   tiny_loop_cb_t cb, struct tiny_clean_ptr data,
					   struct tiny_loop_op **ret)
{
	int fd = -1;
	static const int flags = TFD_NONBLOCK | TFD_CLOEXEC;

	tiny_assert_exec(ret, return -EINVAL);
	switch (timeout.type) {
	case TINY_CLOCK_REALTIME:
		fd = timerfd_create(CLOCK_REALTIME, flags);
		break;
	case TINY_CLOCK_MONOTONIC:
		fd = timerfd_create(CLOCK_MONOTONIC, flags);
		break;
	}
	if (fd < 0) {
		tiny_log_error(loop->log,
			       "failed to create timerfd with error %d", errno);
		return -errno;
	}

	struct timespec ts = to_timespec(timeout.ns);
	struct itimerspec tspec = {ts, ts};
	if (timerfd_settime(fd, 0, &tspec, NULL) != 0) {
		tiny_log_error(loop->log, "failed to set timer with error %d", -errno);
		close(fd);
		return -errno;
	}
	struct tiny_loop_op *op =
	    op_new(loop, (struct op_ifuncs){timer_prep, timer_complete, cb, NULL},
			   TINY_FD(fd, close), data, NULL);
	if (!op)
		return -ENOMEM;
	*ret = op;
	return 0;
}

// Signalfd handling

static int signal_complete(struct tiny_loop_op *op, struct io_uring_cqe *cqe)
{
	if (cqe->res > 0 && cqe->res != sizeof(op->siginfo)) {
		tiny_log_error(op->loop->log, "Partial read of siginfo");
		cqe->res = -EINVAL;
	} else if (cqe->res > 0){
		// lets not bleed the amount of bytes read
		cqe->res = 0;
	}
	return op->f.cb(op, cqe->res);
}

static void signal_prep(struct tiny_loop_op *op, struct io_uring_sqe *sqe)
{
	io_uring_prep_read(sqe, op->fd.fd, &op->siginfo, sizeof(op->siginfo), 0);
}

static int uring_signal(struct tiny_loop *loop, int signal, tiny_loop_cb_t cb,
						struct tiny_clean_ptr data, struct tiny_loop_op **ret)
{
	sigset_t mask;

	tiny_assert_exec(ret, return -EINVAL);
	sigemptyset(&mask);
	sigaddset(&mask, signal);
	int fd = signalfd(-1, &mask, SFD_NONBLOCK | SFD_CLOEXEC);
	sigprocmask(SIG_BLOCK, &mask, NULL);

	if (fd == -1) {
		tiny_log_error(loop->log,  "Failed to get signalfd with error code %d",
					   -errno);
		return -errno;
	}
	struct tiny_loop_op *op =
		op_new(loop, (struct op_ifuncs){signal_prep, signal_complete, cb, NULL},
			   TINY_FD(fd, close), data, NULL);
	if (!op)
		return -ENOMEM;
	*ret = op;
	return 0;
}

static void uring_free(void *ptr)
{
	struct tiny_loop *loop = ptr;
	struct tiny_loop_op *op, *tmp;
	io_uring_queue_exit(&loop->ring);
	uint32_t count = 0;
	// free any pending operations that remain. Should never happen, but
	// always good to check
	tiny_list_foreach_safe(op, tmp, &loop->pending_ops, node) {
	    op_free(op);
		count++;
	}
	if (count)
		tiny_log_error(loop->log, "%p: %u pending operations when freeing",
			loop, count);
	// Free any idle ops. This is basically an indication that an op has not
	// been dereferenced yet
	count = 0;
	tiny_list_foreach_safe(op, tmp, &loop->pending_ops, node) {
	    op_free(op);
		count++;
	}
	if (count)
		tiny_log_error(loop->log,
			"%p: %u idle operations when freeing. Some dereferencing is missing",
			loop, count);

	tiny_list_foreach_safe(op, tmp, &loop->op_pool, node)
		free(op);
	free(loop);
};

int new_uring(struct tiny_context *ctx, struct tiny_value_map *params,
	      struct tiny_clean_ptr *ret)
{
	struct io_uring ring;
	TINY_UNUSED(params);
	// function table
	static const struct tiny_loop_functions funcs = {
		.run = uring_run,
		.stop = uring_stop,
		.read = uring_read,
		.write = uring_write,
		.timer = uring_timer,
		.signal = uring_signal,
	};

	tiny_assert_exec(ctx && ret, return -EINVAL);
	int res;
	struct tiny_logger *logger =
		tiny_context_lookup_system(ctx, tiny_logger, res);
	if (res)
		return res;

	res = io_uring_queue_init(8, &ring, 0);
	if (res) {
		tiny_log_error(logger, "Faile to initialize io_uring queue with %d",
			res);
		return res;
	}

	struct tiny_loop *uring = tiny_alloc(struct tiny_loop);
	*uring = (struct tiny_loop) {
		TINY_DISPATCH(&funcs),
		logger,
		ring,
		true,
		TINY_LIST(uring->op_pool),
		TINY_LIST(uring->pending_ops),
		TINY_LIST(uring->idle_ops),
	};
	*ret = TINY_CLEAN_PTR(uring, uring_free);
	return 0;
}
