// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#include <tiny/interface/loop.h>
#include <tiny/plugin.h>

int new_uring(struct tiny_context *ctx, struct tiny_value_map *params,
	      struct tiny_clean_ptr *ret);

static int tiny_uring_get_factory(uint32_t pos, struct tiny_factory *factory)
{
	if (!factory)
		return -EINVAL;
	switch (pos) {
	case 0: {
		*factory = TINY_FACTORY(tiny_loop, new_uring);
		return 1;
	}
	default:
		break;
	}
	return 0;
}

TINY_PLUGIN_DEFINE("io_uring", tiny_uring_get_factory);
