// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#include "tiny/lib/info.h"

#include <tiny/macros.h>

#include "libtiny_config.h"

TINY_PUBLIC const char *tiny_lib_version() { return LIBTINY_VERSION; }
