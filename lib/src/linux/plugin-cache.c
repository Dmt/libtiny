// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#include "interface/plugin-cache.h"

#include <dlfcn.h>
#include <errno.h>

#include <tiny/alloc.h>
#include <tiny/lib/log.h>
#include <tiny/macros.h>
#include <tiny/ascii/str.h>
#include <tiny/data-structs/list.h>
#include <tiny/structs/dispatch.h>

struct tiny_plugin_cache {
	struct tiny_dispatch d;
	struct tiny_list_node plugins;
};

struct tiny_plugin {
	struct tiny_list_node node;
	char *path;
	void *handle;
};

static int fetch_plugin(struct tiny_plugin_cache *cache, const char *path,
			struct tiny_plugin **ret)
{
	struct tiny_plugin *p;
	tiny_list_foreach(p, &cache->plugins, node)
	{
		if (tiny_streq(p->path, path))
			goto done;
	}
	void *handle = dlopen(path, RTLD_NOW);
	if (!handle) {
		tiny_error("Failed to load plugin with error:'%s'(path:%s)",
			       dlerror(), path);
		return -ENOENT;
	}
	p = tiny_alloc0(struct tiny_plugin);
	tiny_list_init(&p->node);
	p->path = tiny_strdup(path);
	p->handle = handle;
	tiny_list_append(&cache->plugins, &p->node);
done:
	if (ret)
		*ret = p;
	return 0;
}

static int tiny_plugin_cache_lookup_symbol(struct tiny_plugin_cache *cache,
	const char *plugin_path, const char *symbol_name, void **handle)
{
	tiny_assert_exec(cache && plugin_path && symbol_name && handle,
			 return -EINVAL);
	struct tiny_plugin *p;
	int ret = fetch_plugin(cache, plugin_path, &p);
	if (ret)
		return ret;
	*handle = dlsym(p->handle, symbol_name);
	if (!(*handle)) {
		tiny_error("Failed to load symbol with error:'%s'(symbol: %s,path: %s)",
			       dlerror(), symbol_name, plugin_path);
		return -ENOSYS;
	}
	return 0;
}


static void plugin_cache_free(void *ptr)
{
	struct tiny_plugin_cache *cache = ptr;
	struct tiny_plugin *p, *tmp;

	tiny_list_foreach_safe(p, tmp, &cache->plugins, node)
	{
		dlclose(p->handle);
		free(p->path);
		free(p);
	}

	free(cache);
}

int tiny_plugin_cache_new(struct tiny_clean_ptr *ret)
{
	static const struct tiny_plugin_cache_funcs funcs = {
		.lookup = tiny_plugin_cache_lookup_symbol,
	};
	tiny_assert_exec(ret, return -EINVAL);

	struct tiny_plugin_cache *plugin_cache =
		tiny_alloc0(struct tiny_plugin_cache);
	plugin_cache->d = TINY_DISPATCH(&funcs);
	tiny_list_init(&plugin_cache->plugins);
	*ret = TINY_CLEAN_PTR(plugin_cache, plugin_cache_free);
	return 0;
}
