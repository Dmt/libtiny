// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#include <tiny/lib/arg.h>

#include <errno.h>
#include <stddef.h>
#include <stdint.h>

#include <tiny/ascii/buffer.h>
#include <tiny/ascii/str.h>
#include <tiny/lib/info.h>
#include <tiny/lib/log.h>
#include <tiny/macros.h>

#define foreach_option(verb, var)                                              \
	for (var = (struct tiny_arg_option *)(verb->opts);                     \
	     var && (var->c != '\0' || var->long_name != NULL); var++)

#define foreach_verb(vrb, var)                                                 \
	for (var = (struct tiny_arg_verb *)(vrb->adverbs); var && var->name;   \
	     var++)

struct parent_verb {
	const struct tiny_arg_verb *verb;
	struct parent_verb *parent, *child;
};

static void print_help(const struct tiny_arg_verb *verb,
		       const struct parent_verb *parent);
static void print_version(const struct tiny_arg_verb *verb,
			  const struct parent_verb *parent);

static int parse_long_opt(int argc, const char **argv,
						  const struct tiny_arg_verb *verb,
						  const struct parent_verb *parent, void *data)
{
	// for long opts, there can be only one at a time
	const char *str = &argv[0][2];
	struct tiny_arg_option *opt;
	tiny_debug("Parsing long option '%s'", str);
	foreach_option(verb, opt) {
		if (tiny_streq(opt->long_name, str)) {
			if (opt->argument_name) {
				if (argc < 2)
					return -ENODATA;
				int res = verb->parse_option(opt, data, argv[1]);
				return res >= 0 ? 2 : res;
			} else {
				int res = verb->parse_option(opt, data, NULL);
				return res >= 0 ? 1 : res;
			}
		}
	}
	if (tiny_streq(str, "help")) {
		print_help(verb, parent);
		return -EINTR;
	}
	if (tiny_streq(str, "version")) {
		print_version(verb, parent);
		return -EINTR;
	}
	return -EINVAL;
}

static struct tiny_arg_option *
find_short_option(const struct tiny_arg_verb *verb, char chr)
{
	struct tiny_arg_option *opt;
	foreach_option(verb, opt) {
		if (opt->c && opt->c == chr)
			return opt;
	}
	return NULL;
}

static int parse_short_opts(int argc, const char **argv,
							const struct tiny_arg_verb *verb,
							const struct parent_verb *parent, void *data)
{
	int pos = 0;
	const char *str = &argv[0][1];
	tiny_debug("Parsing short option '%s'", str);
	char c;

	// process the rest of the options
	while ((c = str[pos++])) {
		struct tiny_arg_option *opt = find_short_option(verb, c);
		if (opt) {
			if (opt->argument_name) {
				// If the short opt accepts an arg, the rest of the
				// string is the argument. If there is nothing after
				// the short opt, the next argument from argv is used
				if (str[pos] == '\0') {
					if (argc < 2)
						return -ENODATA;
					int res = verb->parse_option(opt, data, argv[1]);
					return res >= 0 ? 2 : res;
				} else {
					int res = verb->parse_option(opt, data, &str[pos]);
					return res >= 0 ? 1 : res;
				}
			}
			// call the option callback with no extra arg
			int res = verb->parse_option(opt, data, NULL);
			if (res < 0)
				return res;
		} else {
			// Do an extra check for the help option
			if (c == 'h') {
				print_help(verb, parent);
				return -EINTR;
			}
			// If we got here, there was an opt we do not understand
			tiny_error("Unhandled command line option '%c'", c);
			return -EINVAL;
		}
	}
	return 1;
}

static int parse_opts(int argc, const char **argv, const struct tiny_arg_verb *verb,
					  const struct parent_verb *parent, void *data)
{
	int ret = 0;
	// We need to have at least one entry
	if (argc < 1)
		return 0;
	// We know we start with a dash from parse_verb. So start from the
	// second character. Also, no need for special checks, we already
	// checked those too.
	if (argv[0][1] == '-')
		ret = parse_long_opt(argc, argv, verb, parent, data);
	else
		ret = parse_short_opts(argc, argv, verb, parent, data);
	return ret;
}

static int parse_verb(int argc, const char **argv, const struct tiny_arg_verb *verb,
		      struct parent_verb *parent, void *data)
{
	int res = 0;
	// we have matched on the first entry, so skip that
	uint16_t consumed = 1;

	tiny_debug("Running parse for verb '%s'", argv[0]);
	// first notify that a verb was found. This give a chance to setup any
	// needed data
	if (verb->on_verb_found)
		data = verb->on_verb_found(data);

	// Start scanning for options, only if we have options
	// We need to do the optional checks here since we still want to check
	// for optionals on verbs that don't have optionals, as well as
	// the special "-" and "--" cases
	tiny_debug("Starting opt check");
	while (argc - consumed > 0) {
		const char *str = argv[consumed];
		// break if not dealing with an optional
		if (str[0] != '-')
			break;
		// the "-" string is actually considered not to be an optional
		if (str[1] == '\0')
			break;
		// the "--" indicates an immediate termimation of all further
		// parsing
		if (str[1] == '-' && str[2] == '\0')
			return -ECANCELED; // Using ECANCELED here to indicate
					   // the termination
		res = parse_opts(argc - consumed, &argv[consumed], verb, parent,
				 data);
		if (res == 0)
			break;
		else if (res < 0)
			return res;
		else
			consumed += res;
	}

	// Run any adverbs
	if (verb->adverbs && argc - consumed > 0) {
		struct parent_verb ourParent = {verb, parent, NULL};
		if (parent)
			parent->child = &ourParent;
		do {
			res = 0;
			// While we have arguments and we consumed data in the
			// last cycle
			tiny_debug("Checking for adverbs c:%d - %d", argc,
				   consumed);
			struct tiny_arg_verb *adverb;
			foreach_verb(verb, adverb)
			{
				if (tiny_streq(argv[consumed], adverb->name)) {
					res = parse_verb(
					    argc - consumed, &argv[consumed],
					    adverb, &ourParent, data);
					tiny_debug("parsed %s, res:%d",
						   adverb->name, res);
					if (res < 0)
						return res;
					else if (res > 0)
						consumed += res;
					else {
						break;
					}
				}
			}
		} while (argc - consumed > 0 && res > 0);
	}
	// Now we are ready to run the verb. But first check that we have enough
	// arguments
	int argno = tiny_strvlen(verb->args);
	if (argc - consumed < argno)
		return -ERANGE;
	res = 0;
	if (verb->on_verb_parsed)
		res = verb->on_verb_parsed(data, &argv[consumed]);
	return res < 0 ? res : consumed + argno;
}

TINY_PUBLIC int tiny_arg_parse(int argc, const char **argv,
			       const struct tiny_arg_config *config, void *data)
{
	// argc can be zero in exotic cases. We error out for now.
	tiny_assert_exec(argc > 0 && argv[0][0] != '\0' && config,
			 return -EINVAL);
	// We create an extra "root" verb for the program. This makes it easy to
	// recursively call parse_verb
	struct tiny_arg_verb prog = {
	    argv[0],	   NULL,	 config->parse_option, NULL,
	    config->verbs, config->opts, config->description, NULL};

	int res = parse_verb(argc, argv, &prog, NULL, data);
	if (res < 0 && res != -ECANCELED)
		return res;
	return argc - res;
}

// PRINT OPERATIONS

static size_t print_opts(struct tiny_buffer *ptr,
			 const struct tiny_arg_verb *verb)
{
	size_t len = 0;
	bool hashelp = false;
	size_t long_len = 7; // "version" is 7 chars long at least
	size_t arg_len = 0;
	len += tiny_buffer_append_printf(ptr, "Options:\n");
	static const char *fmtstr = "    %c%c %s%-*.*s%c%-*.*s %s\n";
	if (verb->opts) {
		struct tiny_arg_option *opt;
		// calc the max long name length
		foreach_option(verb, opt)
		{
			long_len =
			    TINY_MAX(long_len, tiny_strlen(opt->long_name));
			arg_len =
			    TINY_MAX(arg_len, tiny_strlen(opt->argument_name));
		}
		foreach_option(verb, opt)
		{
			bool has_short = opt->c != '\0';
			bool has_long = opt->long_name != NULL;
			bool has_arg = opt->argument_name != NULL;
			len += tiny_buffer_append_printf(
				    ptr, fmtstr,
					has_short ? '-' : ' ',
				    has_short ? opt->c : ' ',
				    has_long ? "--" : "  ", long_len, long_len,
				    has_long ? opt->long_name : "",
				    has_arg ? '=' : ' ', arg_len, arg_len,
				    has_arg ? opt->argument_name : "",
				    opt->description);
			if (opt->c == 'h')
				hashelp = true;
		}
	}
	if (!hashelp)
		len += tiny_buffer_append_printf(
		    ptr, fmtstr, '-', 'h', "--", long_len, long_len, "help",
		    ' ', arg_len, arg_len, "", "Prints this help");

	len += tiny_buffer_append_printf(
	    ptr, fmtstr, ' ', ' ', "--", long_len, long_len, "version", ' ',
	    arg_len, arg_len, "", "Prints version");
	len += tiny_buffer_append_char(ptr, '\n');
	return len;
}

static size_t print_adverbs(struct tiny_buffer *ptr,
			    const struct tiny_arg_verb *verb)
{
	size_t len = 0;
	size_t name_len = 0;
	if (verb->adverbs && verb->adverbs->name) {
		const struct tiny_arg_verb *adverb;
		foreach_verb(verb, adverb)
			name_len = TINY_MAX(name_len, tiny_strlen(adverb->name));
		len += tiny_buffer_append_printf(ptr, "Commands:\n");
		foreach_verb(verb, adverb)
			len += tiny_buffer_append_printf(ptr, "\t%-*.*s %s\n",
							name_len, name_len, adverb->name,
							adverb->description);
		len += tiny_buffer_append_char(ptr, '\n');
	}
	return len;
}

static size_t print_program_desc(struct tiny_buffer *ptr,
				 const struct tiny_arg_verb *verb,
				 const struct parent_verb *parent)
{
	size_t len = 0;
	// Prefix the verb with parent verbs if any
	if (parent) {
		// Climb up to the root parent
		const struct parent_verb *root = parent;
		while (root->parent)
			root = root->parent;
		// now descend, appending each verb
		while (root) {
			len += tiny_buffer_append_printf(ptr, "%s ",
							    root->verb->name);
			root = root->child;
		}
	}
	len += tiny_buffer_append_printf(ptr, verb->name);
	size_t i;
	for (i = 0; i < tiny_strvlen(verb->args); i++)
		len += tiny_buffer_append_printf(ptr, " <%s>", verb->args[i]);
	len += tiny_buffer_append_printf(
	    ptr, " [OPTIONS...]%s\n\n",
	    verb->adverbs ? " [COMMAND [OPTIONS...]]..." : "");
	len += tiny_buffer_append_printf(ptr, "%s\n\n", verb->description);
	len += print_adverbs(ptr, verb);
	len += print_opts(ptr, verb);
	return len;
}

static void print_help(const struct tiny_arg_verb *verb,
		       const struct parent_verb *parent)
{
	// first, call printers with no sized string. This will give us the
	// required allocation size
	size_t len = print_program_desc(NULL, verb, parent);
	tiny_debug("Printing help, chars: %lu", len);
	// Now, call the printers a second time with the allocated string
	TINY_CLEAN_STRUCT(tiny_buffer) ptr = TINY_STR_BUFFER_ALLOC(len);
	print_program_desc(&ptr, verb, parent);
	fputs(tiny_buffer_start(&ptr), stdout);
}

static size_t print_version_desc(struct tiny_buffer *ptr,
				 const char *progname)
{
	size_t len = tiny_buffer_append_printf(ptr, "%s\n", progname);
	len += tiny_buffer_append_printf(
	    ptr, "compiled with tinylib version %s\n", tiny_lib_version());
	return len;
}

static void print_version(const struct tiny_arg_verb *verb,
			  const struct parent_verb *parent)
{

	while (parent) {
		verb = parent->verb;
		parent = parent->parent;
	}
	// First get the size needed for the buffer
	size_t len = print_version_desc(NULL, verb->name);
	// Now, call the printers a second time with the allocated string
	TINY_CLEAN_STRUCT(tiny_buffer) ptr = TINY_STR_BUFFER_ALLOC(len);
	print_version_desc(&ptr, verb->name);
	fputs(tiny_buffer_start(&ptr), stdout);
}
