// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_INTERFACE_PLUGIN__
#define __TINY_INTERFACE_PLUGIN__

#include <tiny/structs/dispatch.h>

// API for loading plugins

// The cache
struct tiny_plugin_cache;

struct tiny_plugin_cache_funcs {
	int (*lookup)(struct tiny_plugin_cache *obj, const char *plugin_path,
				  const char *symbol_name, void **handle);
};

static inline int tiny_plugin_cache_lookup(struct tiny_plugin_cache *cache,
	const char *path, const char *symbol_name, void **handle)
{
	return tiny_dispatch(cache, struct tiny_plugin_cache_funcs,
						 lookup, path, symbol_name, handle);
}
#endif
