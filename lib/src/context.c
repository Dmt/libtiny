// SPDX-FileCopyrightText: 2024 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#include <tiny/lib/context.h>

#include <tiny/value/builder.h>
#include <tiny/value/json.h>
#include <tiny/data-structs/array.h>
#include <tiny/data-structs/bst.h>
#include <tiny/data-structs/list.h>
#include <tiny/sys/fd.h>
#include <tiny/impl/settings.h>
#include <tiny/lib/log.h>
#include <tiny/plugin.h>
#include <tiny/ascii/buffer.h>

#include <unistd.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>

#include "interface/plugin-cache.h"

int tiny_plugin_cache_new(struct tiny_clean_ptr *res);
uint32_t tiny_lib_log_add_logsites(struct tiny_log_site *start,
								   struct tiny_log_site *end);
#define PLUGIN_VERSION_ANY UINT32_MAX
int factory_cmp(const void *l, const void *r)
{
	const struct tiny_factory *lf = l;
	const struct tiny_factory *rf = r;
	int res = strcmp(lf->type.name, rf->type.name);
	if (!res && lf->type.version != PLUGIN_VERSION_ANY &&
		rf->type.version != PLUGIN_VERSION_ANY) {
		// with 2s complement, this subtraction + cast will work
		return (int)(lf->type.version - rf->type.version);
	}
	return res;
}

// we pair systems with their decription for lookups
struct system {
	const struct tiny_type *type;
	struct tiny_clean_ptr mem;
};

struct tiny_context {
	// This needs to be first so we can properly dispatch using just the ref
	struct tiny_dispatch d;
	// We don't expose this for now. Maybe in the future we will
	struct tiny_clean_ptr plugin_cache;
	// the factories used for constructing systems, sorted using a bst
	struct tiny_bst factories;
	// the table of systems.
	struct tiny_array systems;
};

static int add_system(struct tiny_context *ctx, struct tiny_clean_ptr ptr,
	const struct tiny_type *type)
{
	struct system *s = tiny_array_reserve(&ctx->systems, sizeof(struct system));
	if (!s)
		return -ENOMEM;
	s->type = type;
	s->mem = ptr;
	return 0;
}

static int scan_plugin(struct tiny_context *ctx, struct tiny_plugin *p)
{
	// hookup the logging
	tiny_lib_log_add_logsites(p->start, p->end);
	for (uint32_t pos = 0; ; pos++) {
		struct tiny_factory f;
		int res = p->get_factory(pos, &f);
		if (res < 0) {
			tiny_error("get_factory returned error %d", res);
			return res;
		} else if (res == 0) {
			break;
		}
		res = tiny_bst_insert(&ctx->factories, &f);
		if (res) {
			tiny_error("Failed to add factory to BST with error %d", res);
			return res;
		}
		tiny_debug("Added factory %s(version:%u)", f.type.name,
				   f.type.version);
	}
	return 0;
}

static int parse_config_file(struct tiny_span data, struct tiny_clean_ptr *ret) {
	tiny_assert_exec(ret, return -EINVAL);
	// Any changes to the mmap file will cause nasty side effects
	// so we parse the json and copy it to a tiny_value ASAP
	struct tiny_json_token tokens[256] = {};
	struct tiny_json_parser p = TINY_JSON_PARSER(data, tokens);
	int res = tiny_json_parse(&p);
	if (res < 0) {
		tiny_error("Failed to parse json with error %d. pos: '%s'",
			       res, tiny_span_start(&p.buffer));
		return res;
	}
	// we expect to have an object
	if (!tiny_json_is_object(tokens)) {
		tiny_error("Config is expected to contain a json object, token: %s",
				   tokens->data.ptr);
		return -EINVAL;
	}
	TINY_CLEAN_STRUCT(tiny_value_builder) bldr =
		TINY_VALUE_BUILDER(TINY_ARRAY(256));
	res = tiny_value_builder_add_json(&bldr, tokens);
	if (res) {
		tiny_error("Failed to parse jsonwith error '%d'", res);
		return res;
	}
	*ret = tiny_value_builder_pop(&bldr);
	return 0;
}

static int load_config_file(const char *path, struct tiny_clean_ptr *ret)
{
	struct stat s;
	tiny_assert_exec(path && ret, return errno);
	int res = open(path, O_CLOEXEC | O_RDONLY);
	if (res < 0) {
		tiny_error("Failed to open config file with '%d'", res);
		return res;
	}
	TINY_CLEAN_STRUCT(tiny_fd) fd = {res, close};
	// stat file
	res = fstat(fd.fd, &s);
	if (res) {
		tiny_error("Failed to stat config file at '%s'", path);
		return errno;
	} else if (!s.st_size) {
		tiny_error("Empty config file at '%s'", path);
		return -ENODATA;
	}
	// map file
	void *ptr = mmap(NULL, s.st_size, PROT_READ, MAP_PRIVATE, fd.fd, 0);
	if (ptr == MAP_FAILED) {
		tiny_error("Failed to map config file at '%s' of size %lu",
			path, s.st_size);
		return errno;
	}
	// read and parse file
	res = parse_config_file(TINY_SPAN(ptr, s.st_size), ret);
	// unmap
	munmap(ptr, s.st_size);
	return res;
}

static int parse_settings(struct tiny_context *ctx, const struct tiny_value *v)
{
	tiny_assert_exec(ctx && v, return -EINVAL);
	if (v->type != TINY_VALUE_OBJECT) {
		tiny_error("Expected settings to be an object but is of type %u",
			v->type);
		return -EBADF;
	}

	TINY_CLEAN_STRUCT(tiny_clean_ptr) settings = {};
	int res = tiny_impl_settings_init(v, &settings);
	if (res < 0) {
		tiny_error("Failed to init settings with error %d", res);
		return res;
	}
	// load context level settings
	// apply the logging settings
	bool b;
	if (!tiny_settings_fetch(settings.ptr, "log.use-global", &b) && b) {
		// if there is a global logger, install that on the context
		static const struct tiny_type log_type = TINY_LOG_TYPE;
		add_system(ctx, TINY_CLEAN_PTR(_tiny_lib_logger), &log_type);
	}
	const char *s = NULL;
	if (!tiny_settings_fetch(settings.ptr, "log.filter", &s) && s) {
		// TODO: Add log fiter parsing from demo
	}
	// inject settings into context
	static const struct tiny_type settings_type = TINY_SETTINGS_TYPE;
	add_system(ctx, TINY_POP(settings), &settings_type);
	return 0;
}

static int parse_plugins(struct tiny_context *ctx, const struct tiny_value *conf)
{
	tiny_assert_exec(ctx && conf, return -EINVAL);
	if (conf->type != TINY_VALUE_ARRAY) {
		tiny_error("Expected plugins to be an array but is of type %u",
			conf->type);
		return -EBADF;
	}
	// setup a plugin cache
	int res = tiny_plugin_cache_new(&ctx->plugin_cache);
	if (res) {
		tiny_error("Failed to create plugin cache with %d", res);
		return res;
	}

	// go through the list, loading each specified plugin
	struct tiny_value *obj;
	tiny_value_array_foreach(obj, conf) {
		const char *parent = ".";
		struct tiny_value *k, *v, *plgs = NULL;
		const char *key;
		tiny_value_object_foreach(k, v, obj) {
			tiny_value_get(k, &key);
			if (tiny_streq(key, "parent-dir"))
				tiny_value_get(v, &parent);
			else if (tiny_streq(key, "plugins"))
				plgs = v;
		}
		if (!plgs) {
			tiny_warning("plugin object contained no 'plugins' key");
			continue;
		}

		TINY_CLEAN_STRUCT(tiny_buffer) buf = TINY_STR_BUFFER_ALLOC(1023);
		const char *plugin = NULL;

		tiny_value_array_foreach(k, plgs) {
			tiny_buffer_reset(&buf);
			if (!tiny_value_get(k, &plugin)) {
				tiny_buffer_append_printf(&buf, "%s/%s.so", parent, plugin);
				void *symbol = NULL;
				if ((res = tiny_plugin_cache_lookup(ctx->plugin_cache.ptr,
						buf.mem.ptr, "tiny_plugin", &symbol))) {
					tiny_error("Failed to load plugin at '%s' with error %d",
							   (char*)buf.mem.ptr, res);
					return res;
				}
				res = scan_plugin(ctx, symbol);
				if (res)
					return res;
			}
		}
	}
	tiny_bst_sort(&ctx->factories);
	// check for dupliate factories. Quicksort with duplicate keys is random
	// so we will get unpredictable results. If we ever need multiple factories
	// we would have to distinguish between them somehow. So lets not bother
	// with this until we really really have to.
	struct tiny_factory *f;
	const char *last = NULL;
	tiny_bst_foreach(f, &ctx->factories) {
		if (tiny_streq(last, f->type.name)) {
			tiny_error("Duplicate factory in config of type '%s'", last);
			return -EEXIST;
		}
		last = f->type.name;
	}
	return 0;
}

static int parse_systems(struct tiny_context *ctx, const struct tiny_value *syss)
{
	tiny_assert_exec(ctx && syss, return -EINVAL);
	if (syss->type != TINY_VALUE_ARRAY) {
		tiny_error("Expected systems to be an array but is of type %u",
			syss->type);
		return EBADF;
	}
	// go through the list, creating each object
	struct tiny_value *v;
	tiny_value_array_foreach(v, syss) {
		// build the value map
		TINY_CLEAN_STRUCT(tiny_value_map) params = {};
		int res = tiny_value_map_init(v, &params);
		if (res) {
 			tiny_error("Failed to create value map with error %d", res);
			return res;
		}
		// create the system
		res = tiny_context_create(ctx, &params, NULL);
		if (res) {
			tiny_error("Failed to create system with error %d", res);
			return res;
		}
	}
	return 0;
}

static int parse_config(struct tiny_context *ctx, const char *path)
{
	tiny_assert_exec(ctx && path, return -EINVAL);
	// read the conf file
	TINY_CLEAN_STRUCT(tiny_clean_ptr) conf = {};
	int res = load_config_file(path, &conf);
	if (res)
		return res;
	// build the top level value map
	TINY_CLEAN_STRUCT(tiny_value_map) map = {};
	res = tiny_value_map_init(conf.ptr, &map);
	if (res) {
		tiny_error("Failed to map top level object with error '%d'", res);
		return res;
	}

	// parse settings, then plugins, then systems
	struct field {
		struct tiny_span key;
		bool mandatory;
		int (*parse)(struct tiny_context*, const struct tiny_value*);
	} fields[] = {
		{ TINY_STR_SPAN("settings"), true, parse_settings },
		{ TINY_STR_SPAN("plugins"), true, parse_plugins },
		{ TINY_STR_SPAN("systems"), true, parse_systems },

	};
	size_t i;
	for (i = 0; i < TINY_ARRAY_LEN(fields); i++) {
		struct field *f = &fields[i];
		const struct tiny_value *v = tiny_value_map_lookup(&map, f->key);
		if (v) {
			res = f->parse(ctx, v);
			if (res) {
				tiny_error("Failed to parse '%s' with error '%d'", f->key.ptr,
					res);
				return res;
			}
		} else {
			if (f->mandatory) {
				tiny_error("Mandatory field '%s' missing from config",
						   f->key.ptr);
				return -ENOENT;
			}
			tiny_debug("No field named '%s' in config", f->key.ptr);
		}
	}

	return 0;
}

static int impl_find_system(struct tiny_context *ctx, struct tiny_type type,
							void **handle)
{
	tiny_assert_exec(ctx && type.name && handle, return -EINVAL);
	struct system *s;
	tiny_array_foreach(s, &ctx->systems) {
		if (tiny_type_eq(&type, s->type)) {
			*handle = s->mem.ptr;
			return 0;
		}
	}
	return -ENOENT;
}

static int impl_create(struct tiny_context *ctx, struct tiny_value_map *params,
					   void **ret)
{
	struct tiny_factory *f = NULL;
	struct tiny_type type = {NULL, PLUGIN_VERSION_ANY, 0};
	// get the factory related fields from the map
	const struct tiny_value *v = tiny_value_map_lookup(params,
													   TINY_SPAN("type"));
	int res = tiny_value_get(v, &type.name);
	if (res) {
		tiny_error("Failed to get 'type' of system with error %d", res);
		return res;
	}
	v = tiny_value_map_lookup(params, TINY_SPAN("version"));
	res = tiny_value_get(v, &type.version);
	if (res) {
		if (ret) {
			tiny_error("Failed to get 'version' of system with error %d. "
				"You need to specify the version when you need the handle");
			return res;
		}
	}
	// find the factory
	res = tiny_bst_search(&ctx->factories, &type, &f);
	if (res) {
		tiny_error("Failed to find factory with type '%s' with error %d",
				   type.name, res);
		return res;
	} else if (!f || !f->ctor) {
		tiny_error("Got invalid factory for type '%s'", type.name);
		return -EINVAL;
    }
	// invoke the constructor
	TINY_CLEAN_STRUCT(tiny_clean_ptr) ptr;
	res = f->ctor(ctx, params, &ptr);
	if (res) {
		tiny_error("Constructor for type '%s' returned error %d",
				   type.name, res);
		return res;
	}
	// make sure we actually got something
	if (!ptr.ptr) {
		tiny_error("Constructor failed to build system with type '%s'",
				   type.name);
		return -ENOENT;
	}
	if (ret)
		*ret = ptr.ptr;
	return add_system(ctx, TINY_POP(ptr), &f->type);
}

static void context_free(void *ptr)
{
	struct tiny_context *ctx = ptr;
	if (ctx) {
		struct system *s;
		tiny_array_foreach(s, &ctx->systems)
			tiny_clean_ptr_cleanup(&s->mem);
		tiny_bst_cleanup(&ctx->factories);
		tiny_clean_ptr_cleanup(&ctx->plugin_cache);
		free(ctx);
	}
}

int tiny_context_new(const char *cfg_path, struct tiny_context **ret)
{
	static struct tiny_context_funcs funcs = {
		.find_system = impl_find_system,
		.create = impl_create,
	};
	tiny_assert_exec(cfg_path && ret, return -EINVAL);

	struct tiny_context *ctx = tiny_alloc(struct tiny_context);
	*ctx = (struct tiny_context){
		TINY_DISPATCH(&funcs),
		TINY_CLEAN_PTR(),
		// I just picked 16 as a decent block size. This can be tweeked
		TINY_BST(struct tiny_factory, factory_cmp, 16),
		TINY_ARRAY(sizeof(struct system) * 16),
	};
	// the first entity is always the context its self
	static const struct tiny_type ctx_type = TINY_CONTEXT_TYPE;
	add_system(ctx, TINY_CLEAN_PTR(ctx), &ctx_type);
	// parse the conf file
	int res = parse_config(ctx, cfg_path);
	if (res) {
		context_free(ctx);
		return res;
	}
	*ret = ctx;
	return 0;
}

void tiny_context_cleanup_ptr(struct tiny_context **ptr)
{
	if (ptr) {
		context_free(*ptr);
	}
}
