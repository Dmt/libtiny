// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#include <errno.h>
#include <stddef.h>

#include <tiny/lib/log.h>
#include <tiny/ascii/str.h>

// start and end of the "_tiny_debug" section we put
// all the tiny_log_site structs in
extern struct tiny_log_site __start__tiny_debug[];
extern struct tiny_log_site __stop__tiny_debug[];

// All the registered logsites between the application and loaded plugins
static struct tiny_list_node logsites = TINY_LIST(logsites);
// Cache the latest filter applied to filter any newely added logsites
static struct tiny_log_filter latest_filter = TINY_LOG_FILTER_INIT;
// The logger with lifetime handling
struct tiny_clean_ptr global_logger = {};
// the global logger reference for application level logging
TINY_PUBLIC struct tiny_logger *_tiny_lib_logger = NULL;

static bool check_logsite(struct tiny_log_site *site,
						  struct tiny_log_filter *filter)
{
	if (filter->file && !tiny_strmatch(site->file, filter->file))
		return false;
	if (filter->func && !tiny_strmatch(site->func, filter->func))
		return false;
	if (filter->fmt && !tiny_strmatch(site->fmt, filter->fmt))
		return false;
	if (filter->category && !tiny_strmatch(site->category, filter->category))
		return false;
	if (site->line < filter->lstart || site->line > filter->lend)
		return false;
	if (!(site->lvl & filter->lvl))
		return false;
	return true;
}

static int filter_logsite(struct tiny_log_site *site,
						  struct tiny_log_filter *filter)
{
	if (check_logsite(site, filter)) {
		site->enabled = filter->enable;
		site->sigtrap = filter->sigtrap;
		return 1;
	}
	site->enabled = filter->reset ? false : site->enabled;
	return 0;
}

// exposiing this internally so loaded plugins add their own log sites
uint32_t tiny_lib_log_add_logsites(struct tiny_log_site *start,
								   struct tiny_log_site *end)
{
	struct tiny_log_site *it;
	uint32_t count = 0;
	// if the start is part of a list, it has already been registered
	tiny_assert_exec(!tiny_list_has_items(&start->node), return 0);

	for (it = start; it < end; it++) {
		count++;
		tiny_list_append(&logsites, &it->node);
		// filter the newely added site
		filter_logsite(it, &latest_filter);
	}
	return count;
}

TINY_PUBLIC int tiny_lib_log_filter(struct tiny_log_filter filter)
{
	struct tiny_log_site *site;
	int matches = 0;

	tiny_list_foreach(site, &logsites, node)
		matches += filter_logsite(site, &filter);
	// save the latest filter for any future added logsites
	latest_filter = filter;
	return matches;
}

TINY_PUBLIC int tiny_lib_log_install(struct tiny_clean_ptr logger)
{
	_tiny_lib_logger = logger.ptr;
	tiny_clean_ptr_cleanup(&global_logger);
	global_logger = logger;
	// populate the application level callsite if not already set
	uint32_t c = tiny_lib_log_add_logsites(__start__tiny_debug, __stop__tiny_debug);
	if (c)
		tiny_debug("Added %u application level logsites", c);
	return 0;
}
