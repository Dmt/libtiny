// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_LIB_LOG__
#define __TINY_LIB_LOG__

#include <tiny/log.h>
#include <tiny/structs/clean-ptr.h>

#include <limits.h>


extern struct tiny_logger *_tiny_lib_logger;

#define tiny_error(...) tiny_log_error(_tiny_lib_logger, __VA_ARGS__)
#define tiny_warning(...) tiny_log_warning(_tiny_lib_logger, __VA_ARGS__)
#define tiny_info(...) tiny_log_info(_tiny_lib_logger, __VA_ARGS__)
#define tiny_debug(...) tiny_log_debug(_tiny_lib_logger, __VA_ARGS__)

struct tiny_log_filter {
	const char *file;
	const char *func;
	const char *fmt;
	const char *category;
	unsigned int lstart, lend;
	enum tiny_log_level lvl;
	bool reset : 1; // will set callsite to disabled if the filter doesn't match
	bool enable : 1;
	bool sigtrap : 1; // enables sigtraps for matches
};

#define TINY_LOG_FILTER_INIT											\
	(struct tiny_log_filter) {											\
		NULL, NULL, NULL, NULL, 0, UINT_MAX, TINY_LOG_LEVEL_MASK,		\
		    false, false, false											\
	}

// Helper macro to set the log level globally to the specified log mask
#define TINY_LOG_FILTER(level)											\
	(struct tiny_log_filter) {											\
		NULL, NULL, NULL, NULL, 0, UINT_MAX, level,	true, true, false	\
	}

// Filters all logs based on the desired filter.
int tiny_lib_log_filter(struct tiny_log_filter filter);
// Installs a logger as the application level logger. The first time
// calling this will also cause all application level log sites to get added.
int tiny_lib_log_install(struct tiny_clean_ptr logger);
#endif
