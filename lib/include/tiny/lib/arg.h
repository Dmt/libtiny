// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_LIB_ARG__
#define __TINY_LIB_ARG__

#include <stddef.h>
#include <stdbool.h>

/**
 * A simple program argument parser. Blocks of arguments are handled as a series
 * of: <verb> [option [argument]]... Verbs and options are separated by
 * the starting '-'. This allows for complex verbs with their own options
 * to be defined.
 */

#define TINY_ARG_OPTION_TERM                                                   \
	{                                                                      \
		'\0', NULL, NULL, NULL                                        \
	}
#define TINY_ARG_VERB_TERM                                                     \
	{                                                                      \
		NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL                 \
	}

#define TINY_ARG_VERB_ARGS(...) ((const char*[]){__VA_ARGS__, NULL})

struct tiny_arg_option {
	// The identifier for the option. Needs to be a visible ascii character
	const char c;
	// An optional long version of the identifier.
	const char *long_name;
	// Setting this means that the option accepts an argument
	const char *argument_name;
	// description
	const char *description;
};

typedef int (*tiny_arg_parse_option_t)(const struct tiny_arg_option *opt, void *data,
			     const char *arg);

struct tiny_arg_verb {
	// The verb we use to match to this verb. can only contain alphanumeric
	// characters
	const char *name;
	// Callback for when the verb is found, before any arguments or adverbs
	// are parsed if this returns a pointer, it will be passed to options
	// and adverbs. otherwise, the parent pointer is used.
	void *(*on_verb_found)(void *data);
	// Callback to handle options. Will be called once for each option.
	// All options are processed before the verb return a negative error
	// to stop parsing
	tiny_arg_parse_option_t parse_option;
	// Called when a verb, all its options and all its adverbs have been
	// parsed
	int (*on_verb_parsed)(void *data, const char **args);
	// An optional array of adverbs
	const struct tiny_arg_verb *adverbs;
	// An optional array of arguments
	const struct tiny_arg_option *opts;
	// The description for the verb
	const char *description;
	// the list of argument names for the verb. This also determines the
	// number of arguments for the verb.
	const char **args;
};

struct tiny_arg_config {
	const char *description;
	tiny_arg_parse_option_t parse_option;
	const struct tiny_arg_verb *verbs;
	const struct tiny_arg_option *opts;
};

// The parsing function. The root verb name will be set to the program name
int tiny_arg_parse(int argc, const char **argv, const struct tiny_arg_config *info,
		   void *data);

#endif
