// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_LIB_INFO__
#define __TINY_LIB_INFO__

const char *tiny_lib_version();

#endif
