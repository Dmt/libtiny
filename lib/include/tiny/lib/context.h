// SPDX-FileCopyrightText: 2024 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_LIB_CONTEXT__
#define __TINY_LIB_CONTEXT__

#include <tiny/interface/context.h>
#include <tiny/value/map.h>

// An opaque object that encapsulates common operations used as part of the
// lifetime of an application
struct tiny_context;

// creates a new context, loading the config at the specified path
int tiny_context_new(const char *cfg_path, struct tiny_context **ptr);
// cleanup func to be used with TINY_CLEAN_STRUCT_PTR
void tiny_context_cleanup_ptr(struct tiny_context **ptr);

#endif
