<!--
SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>

SPDX-License-Identifier: MIT
-->

# libtiny

This is just a collection of all the simple features I tend to use when writing applications in c in my free time. The project is split into 3 components:

* The header only part of the library
* A tiny static library that provides plugin functionality, argument parsing and a global logger.
* a set of plugins (currently only a loop implemetation in io_uring)

I chose to name this tiny since I want to try and keep the features simple enough that I can maintain them on my own. Maybe I failed at that goal :) 

I intend to continue adding features as I further use the library for my own projects. Long term I hope to be able to use this as an application and daemon development framework.

## Installation
I am currently using this as a meson subproject. You should be able to compile and install as any meson project.

## Demos
The demos folder contains some simple examples of the library features. I will be adding more as I continue to add missing features. 

## Roadmap
Some future goals:
* More plugins! I intend to add plugins for an http server and dbus client. Probably some more system features as well (threading comes to mind).
* CLI implementation

## Contributing
If you feel like you can contribute, make a PR with your suggestions! Just note that this is a hobby project more then anything so I might take time to respond. I use clang-format and reuse for automating licensing. 
