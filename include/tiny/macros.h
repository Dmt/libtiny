// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_MACROS__
#define __TINY_MACROS__

#include <inttypes.h>
#include <stdio.h>

/*
 * A set of macros for making life easier.
 */

#define TINY_BOOLIFY(value) (!!(value))

#define TINY_LIKELY(val) __builtin_expect(TINY_BOOLIFY(val), 1)
#define TINY_UNLIKELY(val) __builtin_expect(TINY_BOOLIFY(val), 0)

#define TINY_FILE_NAME __FILE__
#define TINY_FUNC_NAME __func__
#define TINY_LINE_NUM __LINE__

#define TINY_CONTAINER_OF(ptr, type, member)                                   \
	({                                                                     \
		const typeof(((type *)0)->member) *__mptr = (ptr);             \
		(type *)((char *)__mptr - offsetof(type, member));             \
	})

#define TINY_CONSTRUCTOR(ctor) static void __attribute__((constructor)) ctor();
#define TINY_DESTRUCTOR(dtor) static void __attribute__((destructor)) dtor();

#define TINY_PUBLIC __attribute__((__visibility__("default")))
#define TINY_SECTION(name) __attribute__((used, section(name), aligned(8)))

#define TINY_CLEANUP(x) __attribute__((__cleanup__(x)))
#define TINY_CLEAN_STRUCT(t) TINY_CLEANUP(t##_cleanup) struct t
// I could add the pointer here, but then variable definition doesn't contain
// the pointer, which makes it qu
#define TINY_CLEAN_STRUCT_PTR(t) TINY_CLEANUP(t##_cleanup_ptr) struct t

#define TINY_MIN(l, r)                                                         \
	({                                                                     \
		__auto_type _l = (l);                                          \
		__auto_type _r = (r);                                          \
		_l > _r ? _r : _l;                                             \
	})

#define TINY_MAX(l, r)                                                         \
	({                                                                     \
		__auto_type _l = (l);                                          \
		__auto_type _r = (r);                                          \
		_l > _r ? _l : _r;                                             \
	})

#define TINY_ARRAY_LEN(array) (sizeof(array) / sizeof(array[0]))
#define TINY_UNUSED(v) ((void)(v))

#define TINY_BIT(nth) ((long long int)1 << nth)
#define TINY_BIT_GET(var, nth) ((var & TINY_BIT(nth)) >> nth)

// Typesafe alignemnt macro
#define _TINY_ALIGN_BASE(val, mask) (((val) + mask) & (~mask))
#define TINY_ALIGN(val, alignment)                                             \
	_TINY_ALIGN_BASE(val, ((__typeof__(val))(alignment)-1))
#define TINY_PTR_ALIGN(ptr, alignment) TINY_ALIGN((uintptr_t)ptr, alignment)

#define TINY_PTR_ADD_BASE(ptr, offset) ((uintptr_t)(ptr) + (uintptr_t)(offset))
#define TINY_PTR_ADD(ptr, offset, type) \
	((type*)TINY_PTR_ADD_BASE(ptr, offset))
#define TINY_PTR_DIFF(l, r) ((intptr_t)(l) - (intptr_t)(r))

#define TINY_SWAP(var, new_val)                                                \
	({                                                                     \
		typeof(var) _tmp = (var);                                        \
		(var) = ((typeof(var))(new_val));                                    \
		_tmp;                                                          \
	})

#define TINY_POP(var)							\
	({ typeof(var) _tmp = (var);				\
		memset(&(var), 0x0, sizeof(var)); 		\
		_tmp; })

// One liner check and exec if fail, optimizing towards assuming things work
#ifndef _TINY_ASSERT_HOOK
#define _TINY_ASSERT_HOOK(expr) fprintf(stderr, "%s:%i %s(): Assertion failed" \
	": '%s'\n", TINY_FILE_NAME, TINY_LINE_NUM, TINY_FUNC_NAME, expr);
#endif
#define tiny_assert_exec(expr, code)									\
	do {																\
		if (TINY_UNLIKELY(!(expr))) {									\
			_TINY_ASSERT_HOOK(#expr);							\
			code;														\
		}																\
	} while (0)

#endif
