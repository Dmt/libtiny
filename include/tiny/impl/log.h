// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_IMPL_LOG__
#define __TINY_IMPL_LOG__

#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>

#include <tiny/alloc.h>
#include <tiny/interface/log.h>
#include <tiny/sys/fd.h>
#include <tiny/structs/clean-ptr.h>

// An implementation of a simple logger

struct tiny_logger {
	struct tiny_dispatch d;
	struct tiny_fd fd;
};

static inline char get_level_initial(enum tiny_log_level lvl)
{
	switch (lvl) {
	case TINY_LOG_LEVEL_ERROR:
		return 'E';
	case TINY_LOG_LEVEL_WARNING:
		return 'W';
	case TINY_LOG_LEVEL_INFO:
		return 'I';
	case TINY_LOG_LEVEL_DEBUG:
		return 'D';
	default:
		return '-';
	}
}

static inline int tiny_impl_log_format(struct tiny_log_site *site,
	char *outbuf, size_t outbufsize, va_list *args)
{
	char msg[512];

	const char *basename = strrchr(site->file, '/');
	if (basename)
		basename += 1;
	else
		basename = site->file;
	// first format the message
	vsnprintf(msg, sizeof(msg), site->fmt, *args);
	// now format the message with the extra context
	return snprintf(outbuf, outbufsize, "%c %s[%s:%i %s()]: %s\n",
		 get_level_initial(site->lvl),
		 site->category ? site->category : "", basename, site->line,
		 site->func, msg);
}

static inline int tiny_impl_log(struct tiny_logger *log,
								struct tiny_log_site *site, ...)
{
	char outbuf[1024];

	va_list args;
	va_start(args, site);
	int strsize = tiny_impl_log_format(site, outbuf, sizeof(outbuf), &args);
	va_end(args);
	write(log->fd.fd, outbuf, strsize);
	if (TINY_UNLIKELY(site->sigtrap))
		raise (SIGTRAP);
	return 0;
}

static inline void tiny_impl_log_free(void *ptr)
{
    struct tiny_logger *log = ptr;
    tiny_fd_cleanup(&log->fd);
    free(log);
}

static inline struct tiny_clean_ptr tiny_impl_log_init(struct tiny_fd fd)
{
	static const struct tiny_log_functions funcs = {
	    .log = tiny_impl_log,
	};

	struct tiny_logger *log = tiny_alloc(struct tiny_logger);
	log->d = TINY_DISPATCH(&funcs);
	log->fd = fd;
	return TINY_CLEAN_PTR(log, tiny_impl_log_free);
}

#endif
