// SPDX-FileCopyrightText: 2024 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_IMPL_SETTINGS__
#define __TINY_IMPL_SETTINGS__

#include <tiny/interface/settings.h>
#include <tiny/value/map.h>
#include <tiny/structs/clean-ptr.h>

// An implementation of the settings API using a json file as the backend
struct tiny_settings {
	// needs to be the first member to make it dispatchable
	struct tiny_dispatch d;
	struct tiny_value_map args;
	struct tiny_clean_ptr data;
};

static inline int tiny_impl_settings_lookup(struct tiny_settings *s,
		struct tiny_span k, const struct tiny_value **ret)
{
	tiny_assert_exec(s && tiny_span_valid(&k) && ret, return -EINVAL);
	const struct tiny_value *v = tiny_value_map_lookup(&s->args, k);
	if (!v)
		return -ENOENT;
	*ret = v;
	return 0;
}

static inline void tiny_impl_settings_free(void *ptr)
{
    struct tiny_settings *s = ptr;
	tiny_value_map_cleanup(&s->args);
	tiny_clean_ptr_cleanup(&s->data);
    free(s);
}

static inline int tiny_impl_settings_init(const struct tiny_value *v,
										  struct tiny_clean_ptr *ret)
{
	static struct tiny_settings_funcs funcs = {
	    .lookup = tiny_impl_settings_lookup,
	};

	tiny_assert_exec(v && v->type == TINY_VALUE_OBJECT && ret, return -EINVAL);
	// first, copy the value so we control the lifetime
	TINY_CLEAN_STRUCT(tiny_clean_ptr) cp = TINY_CLEAN_PTR(tiny_value_dup(v),
		free);
	// now, generate the object map
	TINY_CLEAN_STRUCT(tiny_value_map) args = {};
	int res = tiny_value_map_init(cp.ptr, &args);
	tiny_assert_exec(!res, return res);
	// and initialize the settings
	struct tiny_settings *s = tiny_alloc(struct tiny_settings);
	s->d = TINY_DISPATCH(&funcs);
	s->args = TINY_POP(args);
	s->data = TINY_POP(cp);
	*ret = TINY_CLEAN_PTR(s, tiny_impl_settings_free);
	return 0;
}

#endif
