// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_PLUGIN__
#define __TINY_PLUGIN__

#include <tiny/log.h>
#include <tiny/factory.h>

// Function exposing all the implemented factories
typedef int(tiny_plugin_get_factory_t)(uint32_t pos,
				       struct tiny_factory *handle);

// the public symbol exposed on each plugin for accessing its features
struct tiny_plugin {
	const char *name;
	tiny_plugin_get_factory_t *get_factory;
	struct tiny_log_site *start, *end;
};

#define TINY_PLUGIN_DEFINE(name, get_factory)							\
	extern struct tiny_log_site __start__tiny_debug[]					\
	__attribute__((weak, visibility("hidden")));						\
	extern struct tiny_log_site __stop__tiny_debug[]					\
	__attribute__((weak, visibility("hidden")));						\
	TINY_PUBLIC struct tiny_plugin tiny_plugin = {						\
	    name, get_factory, __start__tiny_debug, __stop__tiny_debug}

#endif
