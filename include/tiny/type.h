// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_TYPE__
#define __TINY_TYPE__

#include <tiny/ascii/str.h>

// A simple type descriptor based on the stringified verion of a type identifier
// We use this as part of the plugin API to describe what type a plugin factory
// is exposing and what version of that type it is implementing

// the macro expects that there is a version macro defined for the given type
#define TINY_TYPE(type) \
	((const struct tiny_type){(#type), (TINY_TYPE_VERSION_ ## type), 0})
struct tiny_type {
	const char *name;
	uint32_t version;
	// leaving this initialized to zero, so we can flag in future versions
	// if this struct has extended data
	int32_t _reserved;
};

static inline bool tiny_type_eq(const struct tiny_type *l,
								const struct tiny_type *r)
{
	return tiny_streq(l->name, r->name) && l->version == r->version;
}

#endif
