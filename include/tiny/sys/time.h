// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_TIME__
#define __TINY_TIME__

#include <stdint.h>

// Simple definitions for helping define time

enum tiny_clock_type {
	TINY_CLOCK_REALTIME,
	TINY_CLOCK_MONOTONIC,
};

typedef uint64_t tiny_nsec_t;

#define TINY_SECOND 1000000000lu
#define TINY_MILLISEC 1000000lu
#define TINY_MICROSEC 1000lu

// A timestamp with its associated clock
struct tiny_timestamp {
	const tiny_nsec_t ns;
	const enum tiny_clock_type type;
};

#define TINY_TIMESTAMP_RT(ns)                                                  \
	(struct tiny_timestamp) { ns, TINY_CLOCK_REALTIME }
#define TINY_TIMESTAMP_MONO(ns)                                                \
	(struct tiny_timestamp) { ns, TINY_CLOCK_MONOTONIC }

#endif
