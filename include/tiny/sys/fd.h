// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_FD__
#define __TINY_FD__

#include <tiny/alloc.h>

// rather then pull in unistd.h just declare close
extern int close(int fd);

// A file descriptor wrapper

typedef int(tiny_fd_close_t)(int fd);

struct tiny_fd {
	int fd;
	tiny_fd_close_t *close;
};

static inline int tiny_fd_cleanup(struct tiny_fd *fd)
{
	int res = 0;
	if (fd && fd->fd >= 0 && fd->close) {
		struct tiny_fd tmp = TINY_POP(*fd);
		res = tmp.close(tmp.fd);
	}
	return res;
}

#define TINY_FD0() \
	(struct tiny_fd) { -EBADF, NULL}
#define TINY_FD1(fd)                                                           \
	(struct tiny_fd) { fd, NULL }
#define TINY_FD2(fd, cb)                                                       \
	(struct tiny_fd) { fd, cb }

#define __TINY_FD_GET_MACRO(_0, _1, MACRO, ...) MACRO
#define TINY_FD(...)                                                           \
	__TINY_FD_GET_MACRO(__VA_ARGS__, TINY_FD2, TINY_FD1)(__VA_ARGS__)

static inline int tiny_fd_close_safe(int fd) {
	// protects from stdin, stdout, stderr
	tiny_assert_exec(fd > 2, return -EBADF);
	return close(fd);
}

#endif
