// SPDX-FileCopyrightText: 2024 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_ARRAY__
#define __TINY_ARRAY__

#include <errno.h>
#include <stdlib.h>
#include <tiny/structs/span.h>

// byte array implementation. alloccations always happen in multiple
// of block.
struct tiny_array {
	struct tiny_clean_ptr mem;
	size_t size;
	size_t used;
	size_t block;
};

// An invalid array. This is used to invalidate an array for futher operations
#define TINY_ARRAY_INVAL ((struct tiny_array) {TINY_CLEAN_PTR(), 0, 0, 0})
// sets the deallocator to free so we can realloc the memory ptr freely
#define TINY_ARRAY(blocksize) ((struct tiny_array){TINY_CLEAN_PTR(NULL, free), \
								   0, 0, (blocksize)})
// user defined memory. This will not realloc
#define TINY_ARRAY_STATIC(ptr, len)((struct tiny_array){ TINY_CLEAN_PTR((void*)ptr), \
										(len), (len), 0})

static inline void tiny_array_cleanup(struct tiny_array *arr)
{
	if (arr)
		tiny_clean_ptr_cleanup(&arr->mem);
}

static inline void* tiny_array_start(const struct tiny_array *arr)
{
	return arr->mem.ptr;
}

static inline void* tiny_array_end(const struct tiny_array *arr)
{
	return TINY_PTR_ADD(arr->mem.ptr, arr->used, void);
}

// tries to increase the array size if there is not enough free memory
static inline int tiny_array_maybe_expand(struct tiny_array *arr, size_t len)
{
	tiny_assert_exec(arr, return -EINVAL);
	size_t needed = arr->used + len;
	if (arr->size < needed) {
		// no block size means no realloc
		if (!arr->block)
			return 0;
		void *data = arr->mem.ptr;
		size_t missing = needed - arr->size;
		size_t newSize = arr->size + arr->block * (missing / arr->block + 1);
		// if realloc fails, the original pointer is still valid, so just return
		if (!(data = realloc(data, newSize)))
			return -errno;
		//now we can update the pointer
		arr->mem.ptr = data;
		arr->size = newSize;
	}
	return 1;
}

// reserves memory of the specified size and returns a pointer to it.
static inline void* tiny_array_reserve(struct tiny_array *arr, size_t size)
{
	tiny_assert_exec(size && tiny_array_maybe_expand(arr, size) > 0,
					 return NULL);
	void *ptr = tiny_array_end(arr);
	arr->used += size;
	return ptr;
}

// operations for sequences of the same type
#define tiny_array_foreach(item, arr) \
	for ((item) = tiny_array_start(arr); (item) < (typeof(item))tiny_array_end(arr); (item)++)
#define tiny_array_len(arr, type) ((arr)->used / sizeof(type))
#define tiny_array_n(arr, type, n) (&((type*)tiny_array_start(arr))[n])

#endif
