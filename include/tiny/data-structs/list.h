// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_LIST__
#define __TINY_LIST__

#include <stdbool.h>
#include <stddef.h>

#include <tiny/macros.h>

// A simple doubly linked list implementation

#define TINY_LIST(name)                                                   \
	(struct tiny_list_node) { &(name), &(name) }

struct tiny_list_node {
	struct tiny_list_node *prev, *next;
};

static inline void tiny_list_init(struct tiny_list_node *list)
{
	list->next = list;
	list->prev = list;
}

static inline bool tiny_list_has_items(struct tiny_list_node *list)
{
	return list->next != list;
}

static inline unsigned tiny_list_size(struct tiny_list_node *list)
{
	unsigned count = 0;
	struct tiny_list_node *node = list->next;
	while (node != list) {
		node = node->next;
		count++;
	}
	return count;
}

static inline struct tiny_list_node *
tiny_list_remove(struct tiny_list_node *list)
{
	if (TINY_LIKELY(list)) {
		list->prev->next = list->next;
		list->next->prev = list->prev;
		tiny_list_init(list);
	}
	return list;
}

static inline struct tiny_list_node *
tiny_list_pop_next_node(struct tiny_list_node *list)
{
	if (TINY_LIKELY(list->next != list))
		return tiny_list_remove(list->next);
	return NULL;
}

static inline struct tiny_list_node *
tiny_list_pop_prev_node(struct tiny_list_node *list)
{
	if (TINY_LIKELY(list->prev != list))
		return tiny_list_remove(list->prev);
	return NULL;
}

static inline void tiny_list_append(struct tiny_list_node *list,
				    struct tiny_list_node *toAppend)
{
	// Sanity check that the node to append is not in another list
	tiny_list_remove(toAppend);
	toAppend->prev = list;
	toAppend->next = list->next;
	list->next->prev = toAppend;
	list->next = toAppend;
}

static inline void tiny_list_prepend(struct tiny_list_node *list,
				     struct tiny_list_node *toAppend)
{
	// Sanity check that the node to append is not in another list
	tiny_list_remove(toAppend);
	toAppend->prev = list->prev;
	toAppend->next = list;
	list->prev->next = toAppend;
	list->prev = toAppend;
}

#define tiny_list_pop_next(list, type, member) (tiny_list_has_items(list) ?											\
	TINY_CONTAINER_OF(tiny_list_remove((list)->next), type, member) :	\
	NULL)

#define tiny_list_foreach(node, head, member)                                  \
	for ((node) = TINY_CONTAINER_OF((head)->next, typeof(*node), member);  \
	     &(node)->member != head;                                          \
	     (node) = TINY_CONTAINER_OF((node)->member.next, typeof(*node),    \
					member))

#define tiny_list_foreach_safe(node, tmp, head, member)                        \
	for ((node) = TINY_CONTAINER_OF((head)->next, typeof(*node), member);  \
	     &(node)->member != head &&                                        \
	     ((tmp) = TINY_CONTAINER_OF((node)->member.next, typeof(*node),    \
					member));                              \
	     (node) = (tmp))

#define tiny_list_foreach_node(node, head)                                     \
	for ((node) = (head)->next; (node) != (head); (node) = (node)->next)

#define tiny_list_foreach_node_safe(node, tmp, head)                           \
	for ((node) = (head)->next;                                            \
	     (node) != (head) && ((tmp) = (node)->next); (node) = (tmp))

#define tiny_list_foreach_from_node(node, from, head)                          \
	for ((node) = from; (node) != (head); (node) = (node)->next)

#endif
