// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_RING_BUFFER__
#define __TINY_RING_BUFFER__

#include <stdint.h>
#include <assert.h>

#include <tiny/macros.h>

// A simple ring buffer that relies on pow2 math and unsigned integer overflow
// to make operations nice

struct tiny_ring_buffer {
	uint32_t readpos;
	uint32_t writepos;
	uint32_t mask;
};

static inline struct tiny_ring_buffer tiny_ring_buffer_init(int32_t size)
{
	uint32_t mask = size - 1;
	assert(!(mask & size));
	return (struct tiny_ring_buffer){0, 0, mask};
}

static inline void tiny_ring_buffer_write_advance(struct tiny_ring_buffer *rb,
						 uint32_t add)
{
	__atomic_fetch_add(&rb->writepos, add, __ATOMIC_RELEASE);
}

static inline void tiny_ring_buffer_read_advance(struct tiny_ring_buffer *rb,
						uint32_t add)
{
	__atomic_fetch_add(&rb->readpos, add, __ATOMIC_RELEASE);
}

static inline void tiny_ring_buffer_seek(struct tiny_ring_buffer *rb,
					uint32_t pos)
{
	__atomic_store_n(&rb->readpos, pos, __ATOMIC_RELEASE);
}

uint32_t tiny_ring_buffer_distance(struct tiny_ring_buffer *rb)
{
	return __atomic_load_n(&rb->writepos, __ATOMIC_RELAXED) -
	       __atomic_load_n(&rb->readpos, __ATOMIC_RELAXED);
}

enum tiny_ring_buffer_read_state {
	TINY_RB_OVERRUN = 1,
	TINY_RB_UNDERRUN = 2,
};

// Gets the next contiguous read region. You should call again after advancing
// the read position if you need more memory
static inline int tiny_ring_buffer_read_region(struct tiny_ring_buffer *rb,
					      uint32_t *pos, uint32_t *len)
{
	tiny_assert_exec(rb && pos && len, return -EINVAL);
	uint32_t readpos = __atomic_load_n(&rb->readpos, __ATOMIC_RELAXED);
	uint32_t writepos = __atomic_load_n(&rb->writepos, __ATOMIC_ACQUIRE);
	if (readpos >= writepos)
		return TINY_RB_UNDERRUN;
	uint32_t distance = writepos - readpos;
	if (distance > rb->mask + 1)
		return TINY_RB_OVERRUN;
	writepos = writepos & rb->mask;
	*pos = readpos & rb->mask;
	*len = (*pos < writepos) ? (writepos - *pos) : (rb->mask - *pos + 1);
	return 0;
}

enum tiny_ring_buffer_write_state {
	TINY_RB_WILL_OVERFLOW = 1,
};

// Gets the next contiguous write region. You should call again after advancing
// the write position if you need more memory
static inline int tiny_ring_buffer_write_region(struct tiny_ring_buffer *rb,
					       uint32_t *pos, uint32_t *len)
{
	tiny_assert_exec(rb && pos && len, return -EINVAL);
	uint32_t readpos = __atomic_load_n(&rb->readpos, __ATOMIC_ACQUIRE);
	uint32_t writepos = __atomic_load_n(&rb->writepos, __ATOMIC_RELAXED);
	uint32_t distance = writepos - readpos;
	*pos = writepos & rb->mask;
	readpos = readpos & rb->mask;
	*len = (*pos < readpos) ? (readpos - *pos) : (rb->mask - *pos + 1);
	return distance >= rb->mask + 1 ? TINY_RB_WILL_OVERFLOW : 0;
}

#endif
