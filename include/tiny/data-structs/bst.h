// SPDX-FileCopyrightText: 2024 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_BST__
#define __TINY_BST__

#include <string.h>
#include "array.h"

typedef int (*tiny_cmpf_t)(const void *, const void *);


// custom bsearch. when failing to find a match, it will return a pointer
// to the position the missing key can be inserted
static inline int tiny_bsearch(const void* key, const void *base, uint32_t size,
	uint32_t bytes, tiny_cmpf_t cmp, void **res)
{
	void *pos = (void*)base;
	int c;
	if (!size)
		return -ENODATA;
	tiny_assert_exec(key && base && cmp && res, return -EINVAL);
	// shift size after each cycle rather then upfront so the check is then
	// if size is zero. Also subtracting one element only works if we do that
	// on the pre devided number
	for (; size; size >>= 1) {
		pos = TINY_PTR_ADD(base, (size >> 1) * bytes, void);
		c = cmp(key, pos);
		if (c == 0) {
			*res = pos;
			return 0;
		} else if (c > 0) {
			// we already checked pos, so we trim it from the range
			base = pos = TINY_PTR_ADD(pos, bytes, void);
			size--;
		}
	}
	// the insert point is always in low
	*res = pos;
	return -ENOENT;
}

// Simple binary search tree(BST) using tiny_array for memory and qsort/bsearch
// for implementing the actual operations

struct tiny_bst {
	// storage used for array of entries
	struct tiny_array mem;
	tiny_cmpf_t cmp;
	// The size in bytes of an entry
	uint32_t entry_size;
	bool sorted;
};

#define TINY_BST(type, cmp, items_per_realloc) \
	((struct tiny_bst){TINY_ARRAY(sizeof(type) * (items_per_realloc)), (cmp), \
		sizeof(type), false})

#define TINY_BST_STATIC(ptr, cmp) \
	((struct tiny_bst){TINY_ARRAY_STATIC((ptr), sizeof(ptr)), (cmp), \
		sizeof(*ptr), false})

// For static const types we need to assume that the array is always sorted.
// With sorting disabled, we are never going to modify the array
#define TINY_BST_STATIC_CONST(ptr, cmp) \
	((struct tiny_bst){TINY_ARRAY_STATIC((void*)(ptr), sizeof(ptr)), (cmp), \
		sizeof(*ptr), true})

#define tiny_bst_foreach(ptr, t) \
	tiny_array_foreach(ptr, &(t)->mem)

static inline void tiny_bst_cleanup(struct tiny_bst *tree) {
	if (tree)
		tiny_array_cleanup(&tree->mem);
}

static inline size_t tiny_bst_items(const struct tiny_bst *tree) {
	return tree->mem.used / tree->entry_size;
}

static inline int tiny_bst_sort(struct tiny_bst *tree) {
	tiny_assert_exec(tree, return -EINVAL);
	if (TINY_UNLIKELY(!TINY_SWAP(tree->sorted, true))) {
		qsort(tiny_array_start(&tree->mem), tiny_bst_items(tree),
			  tree->entry_size, tree->cmp);
		return 1;
	}
	return 0;
}

static inline int tiny_bst_search(const struct tiny_bst *tree,
								  void *key, void* ret) {
	tiny_assert_exec(tree && key && ret, return -EINVAL);
	if (tree->sorted) {
		return tiny_bsearch(key, tiny_array_start(&tree->mem),
							tiny_bst_items(tree), tree->entry_size, tree->cmp,
							ret);
	}
	return -EBADR;
};

static inline int tiny_bst_insert(struct tiny_bst *tree, void *entry) {
	tiny_assert_exec(tree && entry, return -EINVAL);
	void* ptr = tiny_array_reserve(&tree->mem, tree->entry_size);
	tiny_assert_exec(ptr, return -ENOMEM);
	memcpy(ptr, entry, tree->entry_size);
	tree->sorted = false;
	return 0;
}

static inline void* tiny_bst_reserve(struct tiny_bst *tree) {
	tiny_assert_exec(tree, return NULL);
	void* ptr = tiny_array_reserve(&tree->mem, tree->entry_size);
	tree->sorted = false;
	return ptr;
}

#endif
