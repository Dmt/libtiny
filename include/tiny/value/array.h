// SPDX-License-Identifier: MIT

#ifndef __TINY_VALUE_ARRAY__
#define __TINY_VALUE_ARRAY__

#include <tiny/value/type.h>

#define TINY_VALUE_ARRAY(v) ((struct tiny_value_array*)(v))

#define tiny_value_array_foreach(v, arr)								\
	for ((v) = tiny_value_array_begin(arr);                             \
         TINY_PTR_ADD(v, sizeof(struct tiny_value), void) <             \
             tiny_value_array_end(arr) &&                               \
             tiny_value_type(v) != TINY_VALUE_UNSET;                    \
		 (v) = TINY_VALUE_NEXT(v))

struct tiny_value_array {
	struct tiny_value v;
    // A series of values follow.
};

static inline void* tiny_value_array_begin(const struct tiny_value *arr) {
    return (arr && arr->type == TINY_VALUE_ARRAY) ?
        TINY_PTR_ADD(arr, sizeof(*arr), void) : NULL;
}

static inline void* tiny_value_array_end(const struct tiny_value *arr) {
    return (arr && arr->type == TINY_VALUE_ARRAY) ? TINY_VALUE_NEXT(arr) : NULL;
}

static inline int tiny_value_array_init(struct tiny_value *v)
{
    tiny_assert_exec(v, return -EINVAL);
    tiny_assert_exec(v->size >= sizeof(struct tiny_value_array), return -ENOMEM);
    struct tiny_value_array arr = {TINY_VALUE(v->size, TINY_VALUE_ARRAY)};
    memcpy(v, &arr, sizeof(arr));
    return 0;
}

static inline struct tiny_value* tiny_value_array_nth(struct tiny_value *v, uint32_t n)
{
    uint32_t i = 0;
    tiny_assert_exec(tiny_value_contains(v, TINY_VALUE_ARRAY,
        sizeof(struct tiny_value_array)), return NULL);
    struct tiny_value *tmp;
    tiny_value_array_foreach(tmp, v) {
        if (i++ == n)
            return tmp;
    }
    return NULL;
}

#endif
