// SPDX-FileCopyrightText: 2024 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_VALUE_MAP__
#define __TINY_VALUE_MAP__

#include <tiny/value/object.h>
#include <tiny/ascii/span.h>
#include <tiny/value/fundamentals.h>
#include <tiny/value/array.h>

// initializers for defining a key-value map
#define TINY_VMAP_KV(k, val) ((struct tiny_value_map_kv){ \
		TINY_SPAN(k), (struct tiny_value*)(&(val))})
#define TINY_VMAP_BOOL(k, b) TINY_VMAP_KV((k), TINY_VALUE_BOOL_INIT(b))
#define TINY_VMAP_INT(k, i) TINY_VMAP_KV((k), TINY_VALUE_INT_INIT(i))
#define TINY_VMAP_DOUBLE(k, d) TINY_VMAP_KV((k), TINY_VALUE_DOUBLE_INIT(d))
#define TINY_VMAP_STRING(k, d) TINY_VMAP_KV((k), TINY_VALUE_STRING_INIT(d))
// static variant
#define TINY_VMAP_KV_STATIC(k, val) ((static struct tiny_value_map_kv){ \
		TINY_SPAN(k), (struct tiny_value*)(&(val))})
#define TINY_VMAP_BOOL_STATIC(k, b) TINY_VMAP_KV_STATIC((k), \
		TINY_VALUE_BOOL_INIT_STATIC(b))
#define TINY_VMAP_INT_STATIC(k, i) TINY_VMAP_KV_STATIC((k), \
		TINY_VALUE_INT_INIT_STATIC(i))
#define TINY_VMAP_DOUBLE_STATIC(k, d) TINY_VMAP_KV_STATIC((k), \
		TINY_VALUE_DOUBLE_INIT_STATIC(d))
#define TINY_VMAP_STRING_STATIC(k, d) TINY_VMAP_KV_STATIC((k), \
		TINY_VALUE_STRING_INIT_STATIC(d))

struct tiny_value_map_kv {
	struct tiny_span k;
	struct tiny_value *v;
};

static inline int tiny_value_map_cmp(const void *lv, const void *rv){
	const struct tiny_value_map_kv *l = lv;
	const struct tiny_value_map_kv *r = rv;
	return tiny_span_strcmp(&l->k, &r->k);
}

#define tiny_value_map_foreach(ptr, map)		\
	tiny_bst_foreach((ptr), &(map)->bst)

#define TINY_VALUE_MAP_STATIC(ptr) ((struct tiny_value_map){TINY_BST_STATIC( \
				(ptr), tiny_value_map_cmp)})

#define TINY_VALUE_MAP_STATIC_CONST(ptr) ((const struct tiny_value_map){ \
			TINY_BST_STATIC_CONST((ptr), tiny_value_map_cmp)})

struct tiny_value_map {
	struct tiny_bst bst;
};

static inline void tiny_value_map_cleanup(struct tiny_value_map *m)
{
	if (m)
		tiny_bst_cleanup(&m->bst);
}

static inline int tiny_value_map_init(struct tiny_value *val,
						struct tiny_value_map *res)
{
	tiny_assert_exec(val && res, return -EINVAL);
	tiny_assert_exec(val->type == TINY_VALUE_OBJECT, return -EBADSLT);
	TINY_CLEAN_STRUCT(tiny_bst) bst = TINY_BST(struct tiny_value_map_kv,
		tiny_value_map_cmp, 16);
	struct tiny_value *k, *v;
	tiny_value_object_foreach(k, v, val) {
		struct tiny_value_map_kv kv = {TINY_VALUE_STR_SPAN(k), v};
		int res = tiny_bst_insert(&bst, &kv);
		if (res)
			return res;
	}
	tiny_bst_sort(&bst);
	*res = (struct tiny_value_map){ TINY_POP(bst) };
	return 0;
}

static inline const struct tiny_value* tiny_value_map_lookup(
	const struct tiny_value_map *map, struct tiny_span key)
{
	tiny_assert_exec(map, NULL);
	struct tiny_value_map_kv *res;
	struct tiny_value_map_kv kv = { key, NULL};
	int r = tiny_bst_search(&map->bst, &kv, &res);
	return r ? NULL : res->v;
}

#endif
