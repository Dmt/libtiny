// SPDX-License-Identifier: MIT

#ifndef __TINY_VALUE_FUNDAMENTALS__
#define __TINY_VALUE_FUNDAMENTALS__

#include <tiny/value/type.h>
#include <tiny/ascii/span.h>

#include <errno.h>

// Generic getter/setter for fundamental types
#define tiny_value_get(tv, val)                                                \
	_Generic((val), \
    bool* : tiny_value_get_bool, \
    int64_t*  : tiny_value_get_int64,  \
    uint32_t*  : tiny_value_get_uint32,  \
    double* : tiny_value_get_double, \
    const char** : tiny_value_get_string \
    )((tv), (val))

#define tiny_value_set(tv, val)						\
	_Generic((val),									\
			 bool : tiny_value_set_bool,			\
			 int : tiny_value_set_int64,			\
			 int64_t  : tiny_value_set_int64,		\
			 double : tiny_value_set_double,		\
			 const char* : tiny_value_strcpy		\
		)((tv), (val))

#define tiny_value_alloc(vl, size)	({									\
			struct tiny_value *val = tiny_value_alloc0(size);			\
			if (tiny_value_set(val, (vl))) {							\
				free(val);												\
				val = NULL;												\
			}															\
			val; })

// boolean type. value is packed into meta[0]
#define TINY_VALUE_BOOL_INIT(b) (struct tiny_value_bool){TINY_VALUE(0, \
			TINY_VALUE_BOOL, 0, (b), 0)}
#define TINY_VALUE_BOOL_INIT_STATIC(b) \
	(static struct tiny_value_bool){TINY_VALUE(0,	\
			TINY_VALUE_BOOL, 0, (b), 0)}
struct tiny_value_bool {
	struct tiny_value v;
};

static inline int tiny_value_get_bool(const struct tiny_value *v, bool *b)
{
	if (!(tiny_value_contains(v, TINY_VALUE_BOOL,
			sizeof(struct tiny_value_bool)) && b))
		return -EINVAL;
	*b = v->meta[0];
	return 0;
}

static inline int tiny_value_set_bool(struct tiny_value *v, bool b)
{
	tiny_assert_exec(v, return -EINVAL);
	struct tiny_value_bool val = {TINY_VALUE(v->size, TINY_VALUE_BOOL)};
	val.v.meta[0] = b;
	memcpy(v, &val, sizeof(val));
	return 0;
}

// integer
#define TINY_VALUE_INT_INIT(i) ((struct tiny_value_int){ \
		TINY_VALUE(sizeof(int64_t), TINY_VALUE_INT), (i)})
#define TINY_VALUE_INT_INIT_STATIC(i) ((static struct tiny_value_int){ \
		TINY_VALUE(sizeof(int64_t), TINY_VALUE_INT), (i)})
struct tiny_value_int {
	struct tiny_value v;
	int64_t i;
};

static inline int tiny_value_get_uint32(const struct tiny_value *v, uint32_t *u)
{
	if (!(tiny_value_contains(v, TINY_VALUE_INT,
							sizeof(struct tiny_value_int)) && u))
		return -EINVAL;
	int64_t i = ((struct tiny_value_int *)(v))->i;
	if (i < 0 || i > UINT32_MAX)
		return -ERANGE;
	*u = i;
	return 0;
}

static inline int tiny_value_get_int64(const struct tiny_value *v, int64_t *i)
{
	if (!(tiny_value_contains(v, TINY_VALUE_INT,
							  sizeof(struct tiny_value_int)) && i))
		return -EINVAL;
	*i = ((struct tiny_value_int *)(v))->i;
	return 0;
}

static inline int tiny_value_set_int64(struct tiny_value *v, int64_t i)
{
	tiny_assert_exec(v, return -EINVAL);
	tiny_assert_exec(v->size >= sizeof(i), return -ENOMEM);
	struct tiny_value_int val = {TINY_VALUE(v->size, TINY_VALUE_INT), i};
	memcpy(v, &val, sizeof(val));
	return 0;
}

// double
#define TINY_VALUE_DOUBLE_INIT(i) ((struct tiny_value_double){ \
		TINY_VALUE(sizeof(double), TINY_VALUE_DOUBLE), (i)})
#define TINY_VALUE_DOUBLE_INIT_STATIC(i) \
	((static struct tiny_value_double){							\
		TINY_VALUE(sizeof(double), TINY_VALUE_DOUBLE), (i)})
struct tiny_value_double {
	struct tiny_value v;
	double d;
};

static inline int tiny_value_get_double(const struct tiny_value *v, double *d)
{
	if (!(tiny_value_contains(v, TINY_VALUE_DOUBLE,
							  sizeof(struct tiny_value_double)) && d))
		return -EINVAL;
	*d = ((struct tiny_value_double *)(v))->d;
	return 0;
}

static inline int tiny_value_set_double(struct tiny_value *v, double d)
{
	tiny_assert_exec(v, return -EINVAL);
	tiny_assert_exec(v->size >= sizeof(d), return -ENOMEM);
	struct tiny_value_double val = {TINY_VALUE(v->size, TINY_VALUE_DOUBLE), d};
	memcpy(v, &val, sizeof(val));
	return 0;
}

// string type. We start from the metadata. This means that a single character
// string (char + nul) will fit in the header
#define TINY_VALUE_STRING_INIT(s) ((struct {							\
	struct tiny_value_header v; char str[sizeof(s)];})					\
		{{sizeof(s) > 1 ? sizeof(s) - 2 : 0, 0, TINY_VALUE_STRING}, (s)})
#define TINY_VALUE_STRING_INIT_STATIC(s) ((static struct {				\
	struct tiny_value_header v; char str[sizeof(s)];})					\
		{{sizeof(s) > 1 ? sizeof(s) - 2 : 0, 0, TINY_VALUE_STRING}, (s)})
#define TINY_VALUE_STR(v)                                                      \
	(TINY_PTR_ADD(v, sizeof(struct tiny_value_string) - 2, char))
#define TINY_VALUE_STR_SPAN(v) (TINY_STR_SPAN(TINY_VALUE_STR(v)))
struct tiny_value_string {
	struct tiny_value v;
};

static inline int tiny_value_get_string(const struct tiny_value *v,
					const char **s)
{
	// 1 char + nul can fit in the header metadata
	if (!(tiny_value_contains(v, TINY_VALUE_STRING,
							  sizeof(struct tiny_value_string)) && s))
		return -EINVAL;
	*s = TINY_VALUE_STR(v);
	return 0;
}

static inline int tiny_value_get_span(const struct tiny_value *v,
									  struct tiny_span *s) {
	if (!(tiny_value_contains(v, TINY_VALUE_STRING,
							  sizeof(struct tiny_value_string)) && s))
		return -EINVAL;
	*s = TINY_VALUE_STR_SPAN(v);
}

static inline uint16_t tiny_value_strlen(const struct tiny_value *v)
{
	tiny_assert_exec(tiny_value_contains(v, TINY_VALUE_STRING,
		sizeof(struct tiny_value_string)), return 0);
	return strlen(TINY_VALUE_STR(v));
}

static inline int tiny_value_strncpy(struct tiny_value *v, const char *s,
					    uint16_t len)
{
	tiny_assert_exec(v && s, return -EINVAL);
	// 2 extra bytes in the metadata in the header
	tiny_assert_exec(v->size + 2 > len, return -ENOMEM);
	struct tiny_value val = TINY_VALUE(v->size, TINY_VALUE_STRING);
	memcpy(v, &val, sizeof(val) - 2);
	tiny_strncpy(TINY_VALUE_STR(v), s, len + 1);
	return 0;
}

static inline int tiny_value_strcpy(struct tiny_value *v, const char *s)
{
	return tiny_value_strncpy(v, s, tiny_strlen(s));
}

#endif
