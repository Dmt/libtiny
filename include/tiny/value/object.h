// SPDX-FileCopyrightText: 2024 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_VALUE_OBJECT__
#define __TINY_VALUE_OBJECT__

#include <tiny/value/type.h>
#include <tiny/data-structs/bst.h>

#define TINY_VALUE_OBJECT(v) ((struct tiny_value_object*)(v))

#define tiny_value_object_foreach(k, v, obj)							\
	for ((k) = tiny_value_object_begin(obj);							\
         (void*)k < tiny_value_object_end(obj) &&						\
			 ((v) = TINY_VALUE_NEXT(k)) &&								\
			 ((void*)v) <= tiny_value_object_end(obj) &&				\
             tiny_value_type(k) == TINY_VALUE_STRING &&                 \
             tiny_value_type(v) != TINY_VALUE_UNSET;					\
		 (k) = TINY_VALUE_NEXT(v))

struct tiny_value_object {
	struct tiny_value v;
	// A series of key-value pairs follows.
};

static inline void* tiny_value_object_begin(struct tiny_value *obj) {
    return (obj && obj->type == TINY_VALUE_OBJECT) ?
		TINY_PTR_ADD(obj, sizeof(*obj), void) : NULL;
}

static inline void* tiny_value_object_end(struct tiny_value *obj) {
    return (obj && obj->type == TINY_VALUE_OBJECT) ? TINY_VALUE_NEXT(obj) : NULL;
}

static inline int tiny_value_object_init(struct tiny_value *v)
{
    tiny_assert_exec(v, return -EINVAL);
    tiny_assert_exec(v->size >= sizeof(struct tiny_value_object),
                     return -ENOMEM);
    struct tiny_value_object obj = {TINY_VALUE(v->size, TINY_VALUE_OBJECT)};
    memcpy(v, &obj, sizeof(obj));
    return 0;
}

#endif
