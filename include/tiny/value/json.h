// SPDX-FileCopyrightText: 2024 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_VALUE_JSON__
#define __TINY_VALUE_JSON__

#include <tiny/json/parser.h>
#include <tiny/value/builder.h>
// tiny_value/json integration

static inline int tiny_value_builder_add_json(struct tiny_value_builder *b,
											  struct tiny_json_token *t)
{
	int res;
	union {
		bool b;
		struct tiny_span ptr;
		int64_t i;
		double d;
	} u;
	if (tiny_json_is_array(t)) {
		TINY_CLEAN_STRUCT(tiny_value_builder_parent) a = {};
		int res = tiny_value_builder_enter_array(b, &a);
		tiny_assert_exec(!res, return res);
		// iterate array
		struct tiny_json_token *v;
		tiny_json_array_foreach(v, t) {
			res = tiny_value_builder_add_json(b, v);
			tiny_assert_exec(!res, return res);
		}
		return 0;
	} else if (tiny_json_is_object(t)) {
		TINY_CLEAN_STRUCT(tiny_value_builder_parent) o = {};
		int res = tiny_value_builder_enter_object(b, &o);
		tiny_assert_exec(!res, return res);
		// iterate object
		struct tiny_json_token *k, *v;
		tiny_json_object_foreach(k, v, t) {
			res = tiny_json_get_string(k, &u.ptr);
			tiny_assert_exec(!res, return res);
			res = tiny_value_builder_add(b, u.ptr);
			tiny_assert_exec(!res, return res);
			res = tiny_value_builder_add_json(b, v);
			tiny_assert_exec(!res, return res);
		}
		return 0;
	} else if (!(res = tiny_json_get_string(t, &u.ptr))) {
		return tiny_value_builder_add(b, u.ptr);
	} else if (!(res = tiny_json_get_bool(t, &u.b))) {
		return tiny_value_builder_add(b, u.b);
	} else if (!(res = tiny_json_get_int(t, &u.i))) {
		return tiny_value_builder_add(b, u.i);
	} else if (!(res = tiny_json_get_double(t, &u.d))) {
		return tiny_value_builder_add(b, u.d);
	} else if (tiny_json_is_null(t)) {
		return tiny_value_builder_add_none(b);
	}
	return -EINVAL;
}

#endif
