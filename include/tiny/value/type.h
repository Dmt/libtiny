// SPDX-License-Identifier: MIT

#ifndef __TINY_VALUE_TYPE__
#define __TINY_VALUE_TYPE__

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>
#include <malloc.h>

#include <tiny/macros.h>

// A generic value type. Aligned with json, not just for easy conversion,
// But to maintain the simplicity of the fundamental types.
// Also allows for easy stack allocation. 64bit aligned.

#define TINY_VALUE_ALIGNMENT 8

enum tiny_value_type {
	// Special type to distinguesh from none so you can tell the difference
	// between an unset type and a user specified null in an array
	TINY_VALUE_UNSET = 0,
	// Fundamentals
	TINY_VALUE_NONE = 1,
	TINY_VALUE_BOOL,
	TINY_VALUE_INT,
	TINY_VALUE_DOUBLE,
	TINY_VALUE_STRING,
	// Complex types
	// an array of elements. Elements don't need to share a type
	TINY_VALUE_ARRAY,
	// Container of key-value pairs
	TINY_VALUE_OBJECT,
	// Extensions
	TINY_VALUE_USER_DEFINED = 0x80, //
	TINY_VALUE_RESERVED = 0xFF, // Reserved for type extension
};

// initializers
#define TINY_VALUE1(size) (struct tiny_value) {(size), 0, TINY_VALUE_UNSET, \
			.meta16 = 0}
#define TINY_VALUE2(size, type) ((struct tiny_value){(size), 0, (type), \
			.meta16 = 0})
#define TINY_VALUE3(size, type, flags)						\
	((struct tiny_value){(size), (flags), (type), .meta16 = 0})
#define TINY_VALUE4(size, type, flags, m16)					\
	((struct tiny_value){(size), (flags), (type), .meta16 = (m16)})
#define TINY_VALUE5(size, type, flags, m1, m2)					\
	((struct tiny_value){(size), (flags), (type), .meta[0] = (m1), \
		.meta[1] = (m2)})


// Variadic initializer
#define __TINY_VALUE_GET_MACRO(_0, _1, _2, _3, _4, MACRO, ...) MACRO
#define TINY_VALUE(...)                                                 \
	__TINY_VALUE_GET_MACRO(__VA_ARGS__,                                 \
		TINY_VALUE5, TINY_VALUE4, TINY_VALUE3, TINY_VALUE2, TINY_VALUE1)		\
	(__VA_ARGS__)

#define TINY_VALUE_SIZE(v) (((struct tiny_value*)(v))->size)
// total value size, including header
#define TINY_VALUE_TOTAL_SIZE(v) (sizeof(struct tiny_value) + TINY_VALUE_SIZE(v))
// ptr to the start of the data region
#define TINY_VALUE_DATA(v) (TINY_PTR_ADD(v, sizeof(struct tiny_value), \
	struct tiny_value))
// Gets the memory address after the end of the value. Useful for iteration
#define TINY_VALUE_NEXT(v) (TINY_PTR_ADD(TINY_VALUE_DATA(v), \
	TINY_VALUE_SIZE(v), struct tiny_value))
// sets v to next value while returning the current value
#define TINY_VALUE_SWAP_NEXT(v) TINY_SWAP(v, TINY_VALUE_NEXT(v))

// Fundamental class for a value. This has to be 8 bytes to fit the alignment
struct tiny_value {
	// The size of the data region after the value struct. This is independent
	// of the size of the type and is typically 64bit aligned
	const uint32_t size;
	// can potentially become a flags field for e.g.
	// flagging a value as read-only
	uint8_t reserved;
	// The type of the value
	uint8_t type;
	// 16bits of metadata. What it is and how it is accessed depends on the type
	union __attribute__((packed)) {
		uint8_t meta[2];
		uint16_t meta16;
	};
};

// The header without the metadata. Used by the string static ctor
struct __attribute__((packed)) tiny_value_header {
	// The size of the data region after the value struct. This is independent
	// of the size of the type and is typically 64bit aligned
	const uint32_t size;
	// can potentially become a flags field for e.g.
	// flagging a value as read-only
	uint8_t reserved;
	// The type of the value
	uint8_t type;
};

static inline struct tiny_value*
tiny_value_static_init(void *buffer, uint32_t size)
{
	tiny_assert_exec(buffer && size >= sizeof(struct tiny_value),
		return NULL);
	struct tiny_value v = TINY_VALUE(size - sizeof(struct tiny_value));
	memcpy(buffer, &v, sizeof(v));
	return (struct tiny_value*)(buffer);
}

static inline uint8_t tiny_value_type(const struct tiny_value *v)
{
	return v ? v->type : TINY_VALUE_UNSET;
}

static inline bool tiny_value_contains(const struct tiny_value *v,
				       uint8_t type, uint32_t needed_mem)
{
	return v && (v->type == type) && (TINY_VALUE_TOTAL_SIZE(v) >= needed_mem);
}

static inline void tiny_value_reset(struct tiny_value *v)
{
	// clear everything except the size of the value.
	memset(TINY_PTR_ADD(v, sizeof(uint32_t), void), 0x0,
		   v->size + sizeof(struct tiny_value) - sizeof(uint32_t));
}

// Helper for copying a value into a memory region
static inline uint32_t tiny_value_memcpy(void *dest, const struct tiny_value *v)
{
	uint32_t size = TINY_VALUE_TOTAL_SIZE(v);
	memcpy(dest, v, size);
	return size;
}

// Allocates a new memory region with the specified size.
static inline struct tiny_value*
tiny_value_alloc0(uint32_t size)
{
	size = TINY_ALIGN(size, TINY_VALUE_ALIGNMENT);
	struct tiny_value *val = malloc(sizeof(struct tiny_value) + size);
	struct tiny_value h = TINY_VALUE(size);
	memcpy(val, &h, sizeof(h));
	return val;
}

static inline struct tiny_value* tiny_value_dup(const struct tiny_value *v) {
	tiny_assert_exec(v, return NULL);
	struct tiny_value *r = tiny_value_alloc0(v->size);
	tiny_value_memcpy(r, v);
	return r;
}

#endif
