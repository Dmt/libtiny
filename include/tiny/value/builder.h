// SPDX-FileCopyrightText: 2024 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_VALUE_BUILDER__
#define __TINY_VALUE_BUILDER__

#include <tiny/data-structs/array.h>
#include <tiny/value.h>

// A helper for building value arrays and objects
struct tiny_value_builder {
	struct tiny_array arr;
};

#define TINY_VALUE_BUILDER(arr) ((struct tiny_value_builder){ (arr) })

static inline void tiny_value_builder_cleanup(struct tiny_value_builder *b)
{
	if (b)
		tiny_array_cleanup(&b->arr);
}

static inline struct tiny_value*
tiny_value_builder_reserve(struct tiny_value_builder *b, uint32_t needed)
{
	//ensure alignment
	tiny_assert_exec(b && needed, return NULL);
    needed = TINY_ALIGN(needed, TINY_VALUE_ALIGNMENT);
	struct tiny_value *vp = tiny_array_reserve(
		&b->arr, needed);
    tiny_assert_exec(vp, return NULL);
    struct tiny_value v = TINY_VALUE(needed - sizeof(struct tiny_value));
    memcpy(vp, &v, sizeof(struct tiny_value));
    return vp;
}

// pops the content of the builder, leaving it in an invalid state
static inline struct tiny_clean_ptr
tiny_value_builder_pop(struct tiny_value_builder *b)
{
	tiny_assert_exec(b, return TINY_CLEAN_PTR());
	// save the memory
	struct tiny_clean_ptr mem = b->arr.mem;
	// Set the array to an unusable state
	b->arr = TINY_ARRAY_INVAL;
	return mem;
}

static inline struct tiny_value*
tiny_value_builder_ptr(struct tiny_value_builder *b)
{
	tiny_assert_exec(b, return NULL);
	return b->arr.mem.ptr;
}

// helper for complex types
struct tiny_value_builder_parent  {
	struct tiny_value_builder *b;
	uint8_t type;
	struct tiny_value *v;
};

static inline void
tiny_value_builder_parent_cleanup(struct tiny_value_builder_parent *p)
{
	if (p) {
		// calculate the container length
		size_t start = TINY_PTR_DIFF(TINY_VALUE_DATA(p->v), p->b->arr.mem.ptr);
		size_t len = p->b->arr.used - start;
		// update the header
		struct tiny_value t = TINY_VALUE(len, p->type);
		memcpy(p->v, &t, sizeof(t));
	}
}

static inline int
tiny_value_builder_enter_container(struct tiny_value_builder *b,
						 struct tiny_value_builder_parent *p, uint8_t type)
{
	tiny_assert_exec(b && p, return -EINVAL);
	// Setup the header
	p->v = tiny_value_builder_reserve(b, sizeof(struct tiny_value));
	tiny_assert_exec(p->v, return -ENOMEM);
	p->b = b;
	p->type = type;
	return 0;
}

static inline int
tiny_value_builder_enter_array(struct tiny_value_builder *b,
							   struct tiny_value_builder_parent *p)
{
	return tiny_value_builder_enter_container(b, p, TINY_VALUE_ARRAY);
}

static inline int
tiny_value_builder_enter_object(struct tiny_value_builder *b,
								struct tiny_value_builder_parent *p)
{
	return tiny_value_builder_enter_container(b, p, TINY_VALUE_OBJECT);
}

static inline int
tiny_value_builder_add_val(struct tiny_value_builder *b,
							  struct tiny_value *v)
{
	tiny_assert_exec(b && v, return -EINVAL);
	struct tiny_value *val = tiny_value_builder_reserve(b,
		TINY_VALUE_TOTAL_SIZE(v));
	tiny_assert_exec(val, return -ENOMEM);
	tiny_value_memcpy(val, v);
	return 0;
}

static inline int tiny_value_builder_add_none(struct tiny_value_builder *b)
{
	tiny_assert_exec(b, return -EINVAL);
	struct tiny_value *val = tiny_value_builder_reserve(b,
		sizeof(struct tiny_value));
	tiny_assert_exec(val, return -ENOMEM);
	return 0;
}

static inline int tiny_value_builder_add_bool(struct tiny_value_builder *b,
	bool v)
{
	tiny_assert_exec(b, return -EINVAL);
	struct tiny_value *val = tiny_value_builder_reserve(b,
		sizeof(struct tiny_value_bool));
	tiny_assert_exec(val, return -ENOMEM);
	return tiny_value_set_bool(val, v);
}

static inline int tiny_value_builder_add_int(struct tiny_value_builder *b,
	int64_t i)
{
	tiny_assert_exec(b, return -EINVAL);
	struct tiny_value *val = tiny_value_builder_reserve(b,
		sizeof(struct tiny_value_int));
	tiny_assert_exec(val, return -ENOMEM);
	return tiny_value_set_int64(val, i);
}

static inline int tiny_value_builder_add_double(struct tiny_value_builder *b,
	double d)
{
	tiny_assert_exec(b, return -EINVAL);
	struct tiny_value *val = tiny_value_builder_reserve(b,
		sizeof(struct tiny_value_double));
	tiny_assert_exec(val, return -ENOMEM);
	return tiny_value_set_double(val, d);
}

static inline int tiny_value_builder_strncpy(struct tiny_value_builder *b,
	char *s, size_t len)
{
	tiny_assert_exec(b && s, return -EINVAL);
	struct tiny_value *val = tiny_value_builder_reserve(b,
		sizeof(struct tiny_value_string) + len);
	tiny_assert_exec(val, return -ENOMEM);
	return tiny_value_strncpy(val, s, len);
}

static inline int tiny_value_builder_add_span(struct tiny_value_builder *b,
	struct tiny_span s)
{
	tiny_assert_exec(b, return -EINVAL);
	struct tiny_value *val = tiny_value_builder_reserve(b,
		sizeof(struct tiny_value_string) + s.len);
	tiny_assert_exec(val, return -ENOMEM);
	return tiny_value_strncpy(val, s.ptr, s.len);
}

static inline int tiny_value_builder_strcpy(struct tiny_value_builder *b,
											char *s)
{
	return tiny_value_builder_strncpy(b, s, tiny_strlen(s));
}

#define tiny_value_builder_add_string(builder, val)			\
	_Generic((val),											\
			 char* : tiny_value_builder_strcpy,				\
			 const char* : tiny_value_builder_strcpy,		\
			 struct tiny_span : tiny_value_builder_add_span	\
		)((builder), (val))


#define tiny_value_builder_add(builder, val)				\
	_Generic((val),											\
			 bool : tiny_value_builder_add_bool,			\
			 int : tiny_value_builder_add_int,				\
			 int64_t  : tiny_value_builder_add_int,			\
			 double : tiny_value_builder_add_double,		\
			 char* : tiny_value_builder_strcpy,				\
			 const char* : tiny_value_builder_strcpy,		\
			 struct tiny_span : tiny_value_builder_add_span	\
		)((builder), (val))

#define tiny_value_builder_add_key_value(b, k, v) ({ \
			int _res = tiny_value_builder_add_string(b, k);	\
			if (!_res)										\
				tiny_value_builder_add(b, v);				\
		})

#endif
