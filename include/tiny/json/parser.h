// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_JSON_PARSER__
#define __TINY_JSON_PARSER__

#include <tiny/ascii/parse.h>
#include <tiny/ascii/parse-int.h>
#include <tiny/ascii/parse-float.h>
#include <tiny/ascii/span.h>
#include <tiny/data-structs/list.h>

struct tiny_json_token {
	struct tiny_list_node parent;
	struct tiny_list_node children;
	struct tiny_span data;
};

static inline bool tiny_json_is_null(struct tiny_json_token *t)
{
	return t ? tiny_span_streq(&t->data, "null") : false;
}

static inline bool tiny_json_is_string(struct tiny_json_token *t)
{
	return t ? tiny_span_char(&t->data, 0) == '"' : false;
}

static inline bool tiny_json_is_array(struct tiny_json_token *t)
{
	return t ? tiny_span_char(&t->data, 0) == '[' : false;
}

static inline bool tiny_json_is_object(struct tiny_json_token *t)
{
	return t ? tiny_span_char(&t->data, 0) == '{' : false;
}

static inline int tiny_json_get_string(struct tiny_json_token *t,
				       struct tiny_span *str)
{
	tiny_assert_exec(t && str, return -EINVAL);
	if (!tiny_json_is_string(t))
		return -EBADR;
	struct tiny_span tmp = t->data;
	tiny_span_pop_front(&tmp, 1);
	tiny_span_pop_back(&tmp, 1);
	*str = tmp;
	return 0;
}

static inline int tiny_json_get_int(struct tiny_json_token *t, int64_t *i)
{
	tiny_assert_exec(t && i, return -EINVAL);
	struct tiny_span s = t->data;
	int res = tiny_parse_i64(&s, 10, i);
	return res > 0 ? s.len ? -EILSEQ : 0
		           : res == 0 ? -EBADR : res;
}

static inline int tiny_json_get_double(struct tiny_json_token *t,
				       double *d)
{
	tiny_assert_exec(t && d && tiny_span_len(&t->data, char),
			 return -EINVAL);
	struct tiny_span s = t->data;
	int res = tiny_parse_double(&s, d);
	return res > 0 ? s.len ? -EILSEQ : 0
		: res == 0 ? -EBADR : res;
}

static inline int tiny_json_get_bool(struct tiny_json_token *t, bool *b)
{
	tiny_assert_exec(t && b, return -EINVAL);
	if (tiny_span_streq(&t->data, "true")) {
		*b = true;
		return 0;
	} else if (tiny_span_streq(&t->data, "false")) {
		*b = false;
		return 0;
	}
	return -EBADR;
}

#define tiny_json_first_child(prnt)                                            \
	TINY_CONTAINER_OF((prnt)->children.next, struct tiny_json_token, parent)

#define tiny_json_next(child)                                                  \
	TINY_CONTAINER_OF((child)->parent.next, struct tiny_json_token, parent)

#define tiny_json_array_foreach(token, prnt)                                   \
	for ((token) = tiny_json_first_child(prnt);                            \
	     &(token)->parent != &(prnt)->children;                            \
	     (token) = tiny_json_next(token))

#define tiny_json_object_foreach(key, value, prnt)                             \
	for ((key) = tiny_json_first_child(prnt),                              \
	    (value) = tiny_json_next(key);                                     \
	     &(key)->parent != &(prnt)->children;                              \
	     (key) = tiny_json_next(value), (value) = tiny_json_next(key))

#define TINY_JSON_PARSER(ptr, tokens)                                          \
	((struct tiny_json_parser){ptr, tokens, 0, TINY_ARRAY_LEN(tokens)})

struct tiny_json_parser {
	struct tiny_span buffer;
	struct tiny_json_token *tokens;
	uint32_t pos, len;
};

static inline struct tiny_json_token* _tiny_json_token_alloc(
	struct tiny_json_parser *p, struct tiny_json_token *parent)
{
	tiny_assert_exec(p->pos < p->len, return NULL);
	struct tiny_json_token *token = &p->tokens[p->pos++];
	tiny_list_init(&token->parent);
	tiny_list_init(&token->children);
	if (parent)
		tiny_list_prepend(&parent->children, &token->parent);
	return token;
}

static inline int _tiny_json_parse_string(
	struct tiny_json_parser *p, struct tiny_json_token *parent)
{
	struct tiny_span *b = &p->buffer;
	uint32_t len = 0;
	char c;
	while ((c = tiny_span_char(b, ++len)) && c != '"') {
		if (c == '\\') {
			switch (tiny_span_char(b, ++len)) {
			case '"':
			case '\\':
			case '/':
			case 'b':
			case 'f':
			case 'n':
			case 'r':
			case 't':
			case 'u':
				break;
			default:
				return -EINVAL;
			}
		}
	}
	if (c != '"')
		return -EINVAL;
	len++;
	struct tiny_json_token *token =_tiny_json_token_alloc(p, parent);
	tiny_assert_exec(token, return -ENOMEM);
	token->data = tiny_span_pop_front(b, len);
	return 1;
}

static inline int tiny_json_parse_step(struct tiny_json_parser *p,
									   struct tiny_json_token *parent)
{
	tiny_assert_exec(p, return -EINVAL);
	tiny_assert_exec(p->pos < p->len, return -ENOMEM);
	struct tiny_span *b = &p->buffer;
	// Munch all the whitespace before a character
	tiny_span_munch_type(b, TINY_ASCII_SPACE);

	struct tiny_json_token *token;
	switch (tiny_span_char(b, 0)) {
	case '\0':
		// no more data left in buffer, lets notify about this
		return -ENOBUFS;
	case '{': {
		// save the start of the sequence to set the span
		void *start = b->ptr;
		tiny_span_pop_front(b, 1);
		// Alloc a token
		token = _tiny_json_token_alloc(p, parent);
		tiny_assert_exec(token, return -ENOMEM);
		// check if it is an empty object
		tiny_span_munch_type(b, TINY_ASCII_SPACE);
		if (tiny_span_munch_char(b, '}')) {
			token->data = TINY_SPAN(start, b->ptr - start);
			return 1;
		}
		// "parse_step" : parse_step ,
		int res = 0;
		do {
			// string
			tiny_span_munch_type(b, TINY_ASCII_SPACE);
			res = _tiny_json_parse_string(p, token);
			tiny_assert_exec(res > 0, return res);
			// then colon
			tiny_span_munch_type(b, TINY_ASCII_SPACE);
			tiny_assert_exec(tiny_span_munch_char(b, ':'), return -EINVAL);
			// then variant
			res = tiny_json_parse_step(p, token);
			tiny_assert_exec(res > 0, return res);
			// then optional comma
			tiny_span_munch_type(b, TINY_ASCII_SPACE);
		} while (tiny_span_munch_char(b, ','));
		// munch the } or error out
		if (!tiny_span_munch_char(b, '}'))
			return -EINVAL;
		// done. Store the entire substring in the token
		token->data = TINY_SPAN(start, b->ptr - start);
	} break;
	case '[': {
		// save the start of the sequence to set the span
		void *start = b->ptr;
		tiny_span_pop_front(b, 1);
		// allocate a new token
		token = _tiny_json_token_alloc(p, parent);
		tiny_assert_exec(token, return -ENOMEM);
		// check if it is an empty array
		tiny_span_munch_type(b, TINY_ASCII_SPACE);
		if (tiny_span_munch_char(b, ']')) {
			token->data = TINY_SPAN(start, b->ptr - start);
			return 1;
		}
		// parse_step ,
		int res = 0;
		do {
			res = tiny_json_parse_step(p, token);
			tiny_assert_exec(res > 0, return res);
			tiny_span_munch_type(b, TINY_ASCII_SPACE);
		} while (tiny_span_munch_char(b, ','));
		// munch the ] or error out
		if (!tiny_span_munch_char(b, ']'))
			return -EINVAL;
		token->data = TINY_SPAN(start, b->ptr - start);
	} break;
	case '"': {
		int res = _tiny_json_parse_string(p, parent);
		tiny_assert_exec(res > 0, return res);
	} break;
	case '-':
	case '0' ... '9':
		token =_tiny_json_token_alloc(p, parent);
		tiny_assert_exec(token, return -ENOMEM);
		token->data =
		    tiny_span_pop_chars(b, "0123456789Ee+-.", b->len);
		break;
	default: {
		const char *str = tiny_span_start(b);
		if (tiny_startswith(str, "true") || tiny_startswith(str, "null")) {
			token =_tiny_json_token_alloc(p, parent);
			tiny_assert_exec(token, return -ENOMEM);
			token->data = tiny_span_pop_front(b, 4);
		} else if (tiny_startswith(str, "false")) {
			token =_tiny_json_token_alloc(p, parent);
			tiny_assert_exec(token, return -ENOMEM);
			token->data = tiny_span_pop_front(b, 5);
		} else {
			return -EINVAL;
		}
	} break;
	}
	return 1;
}

static inline int tiny_json_parse(struct tiny_json_parser *p)
{
	tiny_assert_exec(p, return -EINVAL);
	return tiny_json_parse_step(p, NULL);
}

#endif
