
// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_JSON_BUILDER__
#define __TINY_JSON_BUILDER__

#include <tiny/ascii/buffer.h>

struct tiny_json_builder_parent {
	struct tiny_json_builder *builder;
	struct tiny_json_builder_parent *parent;
	uint32_t children;
	char type;
};

struct tiny_json_builder {
	struct tiny_buffer ptr;
	struct tiny_json_builder_parent *parent;
};

#define TINY_JSON_BUILDER(buf)                                                 \
	(struct tiny_json_builder) { (buf), NULL }
#define TINY_JSON_BUILDER_EMPTY()                                              \
	(struct tiny_json_builder) { TINY_BUFFER_INIT, NULL }

static inline void tiny_json_builder_cleanup(struct tiny_json_builder *b)
{
	if (b)
		tiny_buffer_cleanup(&b->ptr);
}

static inline size_t tiny_json_builder_exit_parent(struct tiny_json_builder *b)
{
	size_t size = 0;
	if (b && b->parent) {
		if (b->parent->type == '{') {
			size = tiny_buffer_append_char(&b->ptr, '}');
		} else {
			size = tiny_buffer_append_char(&b->ptr, ']');
		}
		b->parent = b->parent->parent;
	}
	return size;
}

static inline void
tiny_json_builder_parent_cleanup(struct tiny_json_builder_parent *p)
{
	if (p)
		tiny_json_builder_exit_parent(p->builder);
}

static inline size_t _tiny_json_builder_add_prefix(struct tiny_json_builder *b)
{
	size_t size = 0;
	if (b) {
		if (b->parent && b->parent->children) {
			if (b->parent->type == '{' &&
			    b->parent->children % 2 == 1) {
				size =
				    tiny_buffer_append_char(&b->ptr, ':');
			} else {
				size =
				    tiny_buffer_append_char(&b->ptr, ',');
			}
		} else {
			//size = tiny_buffer_append_char(&b->ptr, ' ');
		}
		if (b->parent)
			b->parent->children++;
	}
	return size;
}

static inline size_t tiny_json_builder_add_null(struct tiny_json_builder *b)
{
	return _tiny_json_builder_add_prefix(b) +
	       tiny_buffer_append_printf(&b->ptr, "null");
}

static inline size_t tiny_json_builder_add_bool(struct tiny_json_builder *b,
						bool v)
{
	return _tiny_json_builder_add_prefix(b) +
	       tiny_buffer_append_printf(&b->ptr, "%s",
					    v ? "true" : "false");
}

static inline size_t tiny_json_builder_add_str(struct tiny_json_builder *b,
					       char *str)
{
	return _tiny_json_builder_add_prefix(b) +
	       tiny_buffer_append_printf(&b->ptr, "\"%s\"", str);
}

static inline size_t tiny_json_builder_add_int(struct tiny_json_builder *b,
					       int64_t i)
{
	return _tiny_json_builder_add_prefix(b) +
	       tiny_buffer_append_printf(&b->ptr, "%ld", i);
}

static inline size_t tiny_json_builder_add_double(struct tiny_json_builder *b,
						  double d)
{
	return _tiny_json_builder_add_prefix(b) +
	       tiny_buffer_append_printf(&b->ptr, "%f", d);
}

#define tiny_json_builder_add(b, val)				\
	_Generic((val),									\
			 bool : tiny_json_builder_add_bool,		\
			 char* : tiny_json_builder_add_str,		\
			 int : tiny_json_builder_add_int,		\
			 int64_t : tiny_json_builder_add_int,	\
			 double : tiny_json_builder_add_double	\
		)((b), (val))

#define tiny_json_builder_add_key_value(b, key, value) \
	({tiny_json_builder_add_str((b), (key)) + tiny_json_builder_add((b), (value));})

static inline size_t
tiny_json_builder_enter_array(struct tiny_json_builder *b,
			      struct tiny_json_builder_parent *p)
{
	size_t size = _tiny_json_builder_add_prefix(b) +
		      tiny_buffer_append_char(&b->ptr, '[');
	p->builder = b;
	p->parent = TINY_SWAP(b->parent, p);
	p->children = 0;
	p->type = '[';
	return size;
}

static inline size_t
tiny_json_builder_enter_object(struct tiny_json_builder *b,
			       struct tiny_json_builder_parent *p)
{
	size_t size = _tiny_json_builder_add_prefix(b) +
		      tiny_buffer_append_char(&b->ptr, '{');
	p->builder = b;
	p->parent = TINY_SWAP(b->parent, p);
	p->children = 0;
	p->type = '{';
	return size;
}

#endif
