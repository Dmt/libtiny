// SPDX-FileCopyrightText: 2024 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_INTERFACE_LOG__
#define __TINY_INTERFACE_LOG__

#include <tiny/type.h>
#include <tiny/log/site.h>
#include <tiny/structs/dispatch.h>

struct tiny_logger;

#define TINY_TYPE_VERSION_tiny_logger 0
#define TINY_LOG_TYPE TINY_TYPE(tiny_logger)

// Logging interface functions
struct tiny_log_functions {
	int (*log)(struct tiny_logger *logger, struct tiny_log_site *site, ...);
};

static inline bool tiny_log_site_is_enabled(struct tiny_logger *logger,
					    struct tiny_log_site *site)
{
	if (TINY_UNLIKELY(!logger))
		return false;
	return site->enabled;
}

// this needs to be a macro to get the proper info into the log site
// Another advantage is that we don't need to add va_arg definitions here
#define tiny_log(logger, level, fmt, ...)								\
	({																	\
		TINY_LOG_SITE_DEFINE((level), (fmt));							\
		if (TINY_UNLIKELY(tiny_log_site_is_enabled((logger), &_info))) { \
			tiny_dispatch(logger, struct tiny_log_functions, log, &_info, \
						  ##__VA_ARGS__);								\
		}																\
	})

#endif
