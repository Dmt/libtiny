// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_INTERFACE_LOOP__
#define __TINY_INTERFACE_LOOP__

#include <tiny/structs/buffer.h>
#include <tiny/structs/dispatch.h>
#include <tiny/sys/fd.h>
#include <tiny/sys/time.h>
#include <tiny/type.h>

// The basic concept for an event loop.
#define TINY_TYPE_VERSION_tiny_loop 0
#define TINY_LOOP_TYPE TINY_TYPE(tiny_loop)
// The loop opaque type
struct tiny_loop;
// opaque type for a loop operation
struct tiny_loop_op;

// operation function table
struct tiny_loop_op_functions {
	// will try to arm the operation, if it isn't already
	int (*arm)(struct tiny_loop_op *op);
	// cancels the running operation
	int (*cancel)(struct tiny_loop_op *op);
	// get the buffer associated with an event
	int (*buffer)(struct tiny_loop_op *op, struct tiny_buffer **buf);
	// getter and setter for attached user data for the operation
	int (*user_data)(struct tiny_loop_op *op, void **data);
	int (*set_user_data)(struct tiny_loop_op *op, struct tiny_clean_ptr data);
	// reduces the reference count. At zero reference, the loop takes over the
	// lifetime of the op
	int (*unref)(struct tiny_loop_op *op);
};

static inline int tiny_loop_op_arm(struct tiny_loop_op *op)
{
	return tiny_dispatch(op, struct tiny_loop_op_functions, arm);
}

static inline int tiny_loop_op_cancel(struct tiny_loop_op *op)
{
	return tiny_dispatch(op, struct tiny_loop_op_functions, cancel);
}

static inline void* tiny_loop_op_buffer(struct tiny_loop_op *op)
{
	struct tiny_buffer *buf = NULL;
	tiny_dispatch(op, struct tiny_loop_op_functions, buffer, &buf);
	return buf;
}

static inline void* tiny_loop_op_user_data(struct tiny_loop_op *op)
{
	void *data = NULL;
	tiny_dispatch(op, struct tiny_loop_op_functions, user_data, &data);
	return data;
}

static inline int tiny_loop_op_set_user_data(struct tiny_loop_op *op,
											 struct tiny_clean_ptr data)
{
	return tiny_dispatch(op, struct tiny_loop_op_functions, set_user_data, data);
}

static inline int tiny_loop_op_unref(struct tiny_loop_op *op)
{
	return tiny_dispatch(op, struct tiny_loop_op_functions, unref);
}

static inline void tiny_loop_op_cleanup_ptr(struct tiny_loop_op **op)
{
	if (op)
		tiny_loop_op_unref(*op);
}

// callback triggered when an operation completes. Return false to drop the
// operation
typedef int(tiny_loop_cb_t)(struct tiny_loop_op *op, int res);

// loop function table
struct tiny_loop_functions {
	// Starts running the loop. The thread running this
	// will only return after a call to stop
	int (*run)(struct tiny_loop *loop);

	// Stops a runnning loop. use this to break from a previous run
	int (*stop)(struct tiny_loop *loop);

	// creates a read operation.
	int (*read)(struct tiny_loop *loop, struct tiny_fd fd,
				struct tiny_buffer *buf, tiny_loop_cb_t cb,
				struct tiny_clean_ptr data, struct tiny_loop_op **res);

	// enqueues a write operation
	int (*write)(struct tiny_loop *loop, struct tiny_fd fd,
				 struct tiny_buffer *buf, tiny_loop_cb_t cb,
				 struct tiny_clean_ptr data, struct tiny_loop_op **res);

	// enqueues a timer
	int (*timer)(struct tiny_loop *loop, struct tiny_timestamp timeout,
				 tiny_loop_cb_t cb, struct tiny_clean_ptr data,
				 struct tiny_loop_op **res);

	// sets a signal handler
	int (*signal)(struct tiny_loop *loop, int sigmask, tiny_loop_cb_t cb,
				  struct tiny_clean_ptr data, struct tiny_loop_op **res);
};

static inline int tiny_loop_run(struct tiny_loop *loop)
{
	return tiny_dispatch(loop, struct tiny_loop_functions, run);
}

static inline int tiny_loop_stop(struct tiny_loop *loop)
{
	return tiny_dispatch(loop, struct tiny_loop_functions, stop);
}

// These are the raw dispatch versions. You need to manually handle arming and
// sinking the references returned by these
static inline int tiny_loop_read_full(struct tiny_loop *loop, struct tiny_fd fd,
	struct tiny_buffer *buf, tiny_loop_cb_t cb, struct tiny_clean_ptr data,
	struct tiny_loop_op **ref)
{
	return tiny_dispatch(loop, struct tiny_loop_functions,
						 read, fd, buf, cb, data, ref);
}

static inline int tiny_loop_write_full(struct tiny_loop *loop, struct tiny_fd fd,
	struct tiny_buffer *buf, tiny_loop_cb_t cb, struct tiny_clean_ptr data,
	struct tiny_loop_op **ref)
{
	return tiny_dispatch(loop, struct tiny_loop_functions,
						 write, fd, buf, cb, data, ref);
}

static inline int tiny_loop_timer_full(struct tiny_loop *loop,
	struct tiny_timestamp timeout, tiny_loop_cb_t cb, struct tiny_clean_ptr data,
	struct tiny_loop_op **ref)
{
	return tiny_dispatch(loop, struct tiny_loop_functions,
						 timer, timeout, cb, data, ref);
}

static inline int tiny_loop_signal_full(struct tiny_loop *loop, int sigmask,
	tiny_loop_cb_t cb, struct tiny_clean_ptr data, struct tiny_loop_op **ref)
{
	return tiny_dispatch(loop, struct tiny_loop_functions,
						 signal, sigmask, cb, data, ref);
}

// these are the standalone versions that create a new operation, arm it and
// drop the reference so it is managed by the loop
static inline int tiny_loop_read(struct tiny_loop *loop, struct tiny_fd fd,
	struct tiny_buffer *buf, tiny_loop_cb_t cb, struct tiny_clean_ptr data)
{
	TINY_CLEAN_STRUCT_PTR(tiny_loop_op) *op;
	int res =  tiny_loop_read_full(loop, fd, buf, cb, data, &op);
	if (res)
		return res;
	return tiny_loop_op_arm(op);
}

static inline int tiny_loop_write(struct tiny_loop *loop, struct tiny_fd fd,
	struct tiny_buffer *buf, tiny_loop_cb_t cb, struct tiny_clean_ptr data)
{
	TINY_CLEAN_STRUCT_PTR(tiny_loop_op) *op;
	int res =  tiny_loop_write_full(loop, fd, buf, cb, data, &op);
	if (res)
		return res;
	return tiny_loop_op_arm(op);
}

static inline int tiny_loop_timer(struct tiny_loop *loop,
	struct tiny_timestamp timeout, tiny_loop_cb_t cb, struct tiny_clean_ptr data)
{
	TINY_CLEAN_STRUCT_PTR(tiny_loop_op) *op;
	int res =  tiny_loop_timer_full(loop, timeout, cb, data, &op);
	if (res)
		return res;
	return tiny_loop_op_arm(op);
}

static inline int tiny_loop_signal(struct tiny_loop *loop, int sigmask,
								   tiny_loop_cb_t cb, struct tiny_clean_ptr data)
{
	TINY_CLEAN_STRUCT_PTR(tiny_loop_op) *op;
	int res =  tiny_loop_signal_full(loop, sigmask, cb, data, &op);
	if (res)
		return res;
	return tiny_loop_op_arm(op);
}

#endif
