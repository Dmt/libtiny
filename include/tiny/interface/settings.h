// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_INTERFACE_SETTINGS__
#define __TINY_INTERFACE_SETTINGS__

#include <tiny/structs/span.h>
#include <tiny/value/type.h>
#include <tiny/value/fundamentals.h>
#include <tiny/structs/dispatch.h>
#include <tiny/type.h>

// API for retrieving settings
struct tiny_settings;

#define TINY_TYPE_VERSION_tiny_settings 0
#define TINY_SETTINGS_TYPE TINY_TYPE(tiny_settings)

struct tiny_settings_funcs {
	// does a lookup, returning the value if found. You should not
	int (*lookup)(struct tiny_settings *obj, struct tiny_span key,
				  const struct tiny_value** val);
};

static inline int tiny_settings_lookup(struct tiny_settings *s,
	struct tiny_span key, const struct tiny_value** val)
{
	return tiny_dispatch(s, struct tiny_settings_funcs, lookup, key, val);
}

#define tiny_settings_fetch(handle, key, val) ({						\
			const struct tiny_value *_v;								\
			int _res = tiny_settings_lookup(handle, TINY_STR_SPAN(key), &_v); \
			if (!_res)													\
				_res = tiny_value_get(_v, val);							\
			_res;														\
	})

#endif
