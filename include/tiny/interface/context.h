// SPDX-FileCopyrightText: 2024 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_INTERFACE_CONTEXT__
#define __TINY_INTERFACE_CONTEXT__

#include <tiny/structs/dispatch.h>
#include <tiny/value/map.h>
#include <tiny/type.h>

struct tiny_context;

#define TINY_TYPE_VERSION_tiny_context 0
#define TINY_CONTEXT_TYPE TINY_TYPE(tiny_context)
// A context provides access to fundamental primitives for both the application
// and plugins
struct tiny_context_funcs {
	// tries to lookup a system, based on the desired metadata
	int (*find_system)(struct tiny_context *ptr, struct tiny_type type,
					   void **handle);

	// creates a new entity with the desired properties
	int (*create)(struct tiny_context *ptr, struct tiny_value_map *params,
				  void **handle);
};

// convenience wrapper to uniformly lookup the right type with the correct
// version and cast the result to the same type
#define tiny_context_lookup_system(ctx, type, res) ({							\
			void *_ptr = NULL;											\
			res = tiny_context_find_system(ctx, TINY_TYPE(type), &_ptr); \
			(struct type*)(_ptr);										\
	})

static inline int tiny_context_find_system(struct tiny_context *ctx,
		struct tiny_type type, void **handle) {
	return tiny_dispatch(ctx, struct tiny_context_funcs,
						 find_system, type, handle);
}

static inline int tiny_context_create(struct tiny_context *ctx,
		struct tiny_value_map *params, void **handle) {
	return tiny_dispatch(ctx, struct tiny_context_funcs,
						 create, params, handle);
}

#endif
