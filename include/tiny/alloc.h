// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_ALLOC__
#define __TINY_ALLOC__

#include <stdlib.h>
#include <string.h>

// Simple allocation helpers

#define tiny_clear(ptr) (memset(ptr, 0x0, sizeof(*ptr)))

#define tiny_heapify(var) ({                                \
            typeof(var) *_tmp = tiny_alloc(typeof(var));    \
            memcpy(_tmp, &(var), sizeof(var));              \
            _tmp;                                           \
        })

#define tiny_alloc(type) ((type *)malloc(sizeof(type)))

#define tiny_alloc0(type) ((type *)calloc(1, sizeof(type)))

#define tiny_malloc0(bytes) (calloc(1, bytes))

#define tiny_alloc_array(type, num) ((type *)malloc(sizeof(type) * num))

#define tiny_alloc_array0(type, num) ((type *)calloc((num), sizeof(type)))

static inline void __tiny_cleanup_free(void *p) { free(*(void **)p); }
#define TINY_AUTO_FREE __attribute__((__cleanup__(__tiny_cleanup_free)))

#endif
