// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_STRUCTS_SPAN__
#define __TINY_STRUCTS_SPAN__

#include <tiny/macros.h>
#include <tiny/structs/clean-ptr.h>

// A reference to a contiguous sequence of memory
struct tiny_span {
	// Pointer to the actual memory
	void* ptr;
	// span length
	size_t len;
};

#define TINY_SPAN_EMPTY() \
	((struct tiny_span){NULL, 0})
#define TINY_SPAN_INIT1(ptr) \
	((struct tiny_span){(ptr), sizeof(ptr)})
#define TINY_SPAN_INIT2(ptr, len)							\
	((struct tiny_span){(ptr), (len)})

// Variadic constructors
#define __TINY_SPAN_GET_MACRO(_0, _1, _2, MACRO, ...) MACRO
#define TINY_SPAN(...)											 \
	__TINY_SPAN_GET_MACRO(_0 __VA_OPT__(,)##__VA_ARGS__, TINY_SPAN_INIT2, \
						  TINY_SPAN_INIT1, TINY_SPAN_EMPTY)		\
	(__VA_ARGS__)

// operations for sequences of the same type
#define tiny_span_foreach(item, span) \
	for ((item) = tiny_span_start(span); item < tiny_span_end(arr); (item)++)
#define tiny_span_len(span, type) ((span)->len / sizeof(type))
#define tiny_span_n(span, type, n) ((type*)(tiny_span_start(span))[n])

// basic functions
static inline bool tiny_span_valid(const struct tiny_span *ptr)
{
	return TINY_LIKELY(ptr) ? TINY_BOOLIFY(ptr->ptr) : false;
}

// Get a pointer to the start of the valid memory region
static inline void *tiny_span_start(const struct tiny_span *ptr)
{
	return TINY_LIKELY(ptr) ? ptr->ptr : NULL;
}

static inline void *tiny_span_end(const struct tiny_span *ptr)
{
	return TINY_LIKELY(tiny_span_valid(ptr))
		   ? TINY_PTR_ADD(ptr->ptr, ptr->len, void)
		   : NULL;
}

// byte-wise operations
static inline uint8_t tiny_span_byte(const struct tiny_span *ptr,
					   size_t pos)
{
	return TINY_LIKELY(tiny_span_valid(ptr) && ptr->len > pos)
		   ? *TINY_PTR_ADD(tiny_span_start(ptr), pos, uint8_t)
		   : 0;
}

static inline struct tiny_span
tiny_span_subspan(struct tiny_span *ptr, size_t offset, size_t len)
{
	if (!tiny_span_valid(ptr) || offset > ptr->len)
		return TINY_SPAN_EMPTY();
	len = TINY_MIN(len, ptr->len - offset);
	return TINY_SPAN_INIT2(TINY_PTR_ADD(ptr->ptr, offset, void), len);
}

static inline struct tiny_span
tiny_span_pop_front(struct tiny_span *ptr, size_t len)
{
	len = TINY_MIN(tiny_span_len(ptr, uint8_t), len);
	if (!len)
		return TINY_SPAN_EMPTY();
	void *p = tiny_span_start(ptr);
	*ptr = TINY_SPAN_INIT2(TINY_PTR_ADD(p, len, void), ptr->len - len);
	return TINY_SPAN_INIT2(p, len);
}

static inline struct tiny_span
tiny_span_pop_back(struct tiny_span *ptr, size_t len)
{
	len = TINY_MIN(tiny_span_len(ptr, uint8_t), len);
	tiny_assert_exec(len, return TINY_SPAN_EMPTY());
	void *s = tiny_span_start(ptr);
	*ptr = TINY_SPAN_INIT2(s, ptr->len - len);
	return TINY_SPAN_INIT2(TINY_PTR_ADD(s, ptr->len, void), len);
}

#endif
