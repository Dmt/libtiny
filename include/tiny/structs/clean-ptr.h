// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_STRUCTS_CLEAN_PTR__
#define __TINY_STRUCTS_CLEAN_PTR__

#include <stdbool.h>
#include <stddef.h>

#include <tiny/macros.h>

// The type signature of a generic free function. Might want to move this
// somewhere else
typedef void(tiny_free_t)(void *p);

// A pointer that knows how to tidy its self up
struct tiny_clean_ptr {
	void *ptr;
	tiny_free_t *cleanup;
};

#define TINY_CLEAN_PTR_EMPTY()                                                 \
	(struct tiny_clean_ptr) { NULL, NULL }
#define TINY_CLEAN_PTR_INIT1(data)                                             \
	(struct tiny_clean_ptr) { data, NULL }
#define TINY_CLEAN_PTR_INIT2(data, cleanup)                                    \
	(struct tiny_clean_ptr) { data, cleanup }

// Variadic constructor
#define __TINY_CLEAN_PTR_GET_MACRO(_0, _1, _2, MACRO, ...) MACRO
#define TINY_CLEAN_PTR(...)                                                    \
	__TINY_CLEAN_PTR_GET_MACRO(_0 __VA_OPT__(, )##__VA_ARGS__,             \
				   TINY_CLEAN_PTR_INIT2, TINY_CLEAN_PTR_INIT1, \
				   TINY_CLEAN_PTR_EMPTY)                       \
	(__VA_ARGS__)

static inline bool tiny_clean_ptr_valid(struct tiny_clean_ptr *ptr)
{
	return ptr ? TINY_BOOLIFY(ptr->ptr) : false;
}

static inline bool tiny_clean_ptr_has_free_func(struct tiny_clean_ptr *ptr)
{
	return ptr ? TINY_BOOLIFY(ptr->cleanup) : false;
}

static inline void *tiny_clean_ptr_get(struct tiny_clean_ptr *ptr)
{
	return ptr ? ptr->ptr : NULL;
}

static inline void tiny_clean_ptr_cleanup(struct tiny_clean_ptr *ptr)
{
	if (ptr->ptr) {
		// We swap out the old pointers to break any potential cycles
		// where the cleanup calls back into this code
		tiny_free_t *cb = TINY_SWAP(ptr->cleanup, NULL);
		if (cb)
			cb(TINY_SWAP(ptr->ptr, NULL));
	}
}

#endif // __TINY_STRUCTS_FUNC__
