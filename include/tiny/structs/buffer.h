// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_BUFFER__
#define __TINY_BUFFER__

#include <tiny/alloc.h>
#include <tiny/structs/span.h>

// A minimal buffer wrapper with basic sematics for defining and accessing
// a valid region of memory.
struct tiny_buffer {
	struct tiny_clean_ptr mem;
	// Total region size
	size_t size;
	// The offset from the beginning of the valid region
	size_t offset;
	// The length of the valid region, starting from the offset
	size_t len;
};

#define TINY_BUFFER_INIT ((struct tiny_buffer){{}, 0, 0, 0})
#define TINY_BUFFER_EMPTY2(ptr, size)									\
	((struct tiny_buffer){TINY_CLEAN_PTR(ptr), (size), 0, 0})
#define TINY_BUFFER_EMPTY3(ptr, size, _cleanup)							\
	((struct tiny_buffer){(TINY_CLEAN_PTR(ptr, _cleanup)), (size), 0, 0})
#define TINY_BUFFER(ptr, size)                                           \
	((struct tiny_buffer){TINY_CLEAN_PTR(ptr), (size), 0, (size)})

static inline struct tiny_buffer tiny_buffer_new(size_t size)
{
	return TINY_BUFFER_EMPTY3(calloc(1, size), size, free);
}

static inline struct tiny_buffer tiny_buffer_cleanup(struct tiny_buffer *buf)
{
	if (TINY_LIKELY(buf))
		tiny_clean_ptr_cleanup(&buf->mem);
	return TINY_BUFFER_INIT;
}

static inline bool tiny_buffer_valid(struct tiny_buffer *buf)
{
	return buf ? buf->mem.ptr : false;
}

static inline void tiny_buffer_reset(struct tiny_buffer *buf)
{
	if (TINY_LIKELY(buf))
		buf->len = buf->offset = 0;
}

static inline void *tiny_buffer_start(struct tiny_buffer *buf)
{
	return tiny_buffer_valid(buf) ?
		TINY_PTR_ADD(buf->mem.ptr, buf->offset, void) : NULL;
}

static inline void *tiny_buffer_end(struct tiny_buffer *buf)
{
	return tiny_buffer_valid(buf) ?
		TINY_PTR_ADD(buf->mem.ptr, buf->offset + buf->len, void) : NULL;
}

static inline size_t tiny_buffer_len(struct tiny_buffer *buf)
{
	return buf && buf->mem.ptr ? buf->len : 0;
}

static inline bool tiny_buffer_is_empty(struct tiny_buffer *buf)
{
	return !tiny_buffer_len(buf);
}

// wrapper around tiny_buffer_end to pair semantics with padding_len
static inline void *tiny_buffer_padding(struct tiny_buffer *buf)
{
	return tiny_buffer_end(buf);
}

static inline size_t tiny_buffer_padding_len(struct tiny_buffer *buf)
{
	return buf ? buf->size - buf->offset - buf->len : 0;
}

// Advances the beginning of the buffer by a number of bytes
static inline size_t tiny_buffer_trim_start(struct tiny_buffer *buf,
					       size_t len)
{
	if (buf) {
		len = TINY_MIN(buf->len, len);
		buf->offset += len;
		buf->len -= len;
		return len;
	}
	return 0;
}

// Trims len bytes off the end of the valid region of the buffer
static inline size_t tiny_buffer_trim_end(struct tiny_buffer *buf,
					      size_t len)
{
	if (buf) {
		len = TINY_MIN(buf->len, len);
		buf->len -= len;
		return len;
	}
	return 0;
}

// truncates the buffer, setting its size to len
static inline void tiny_buffer_truncate(struct tiny_buffer *buf, size_t len)
{
	if (buf)
		buf->len = TINY_MIN(buf->len, len);
}

static inline struct tiny_span
tiny_buffer_pop_start(struct tiny_buffer *buf, size_t len)
{
	void *p = tiny_buffer_start(buf);
	len = tiny_buffer_trim_start(buf, len);
	return TINY_SPAN_INIT2(p, len);
}

static inline struct tiny_span
tiny_buffer_pop_end(struct tiny_buffer *buf, size_t len)
{
	// first trim so end is at the start of the popped segment
	len = tiny_buffer_trim_end(buf, len);
	void *p = tiny_buffer_end(buf);
	return TINY_SPAN_INIT2(p, len);
}

// Increases the size of the buffer by len bytes, up to the available extra
// bytes at the start of the buffer
static inline size_t tiny_buffer_extend_start(struct tiny_buffer *buf,
						 size_t len)
{
	if (buf) {
		len = TINY_MIN(buf->offset, len);
		buf->offset -= len;
		buf->len += len;
		return buf->len;
	}
	return 0;
}

// Extends the end of the valid region, reducing the padding by len
static inline size_t tiny_buffer_extend_end(struct tiny_buffer *buf, size_t len)
{
	if (buf) {
		buf->len = TINY_MIN(buf->size - buf->offset - buf->len, buf->len + len);
		return buf->len;
	}
	return 0;
}

static inline bool tiny_buffer_resize(struct tiny_buffer *buf, size_t offset,
				      size_t len)
{
	if (!buf || buf->size < len || buf->size - len < offset)
		return false;
	buf->offset = offset;
	buf->len = len;
	return true;
}

static inline struct tiny_span tiny_buffer_subspan(struct tiny_buffer *buf,
												   size_t offset, size_t len)
{
	if (!buf)
		return TINY_SPAN_EMPTY();
	offset = TINY_MIN(offset, buf->len);
	len = TINY_MIN(len, buf->len - offset);
	return TINY_SPAN_INIT2(TINY_PTR_ADD(buf->mem.ptr, buf->offset + offset,
										void), len);
}

static inline struct tiny_span tiny_buffer_span(struct tiny_buffer *buf)
{
	return tiny_buffer_subspan(buf, 0, -1);
}

#endif
