// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_STRUCTS_DISPATCH__
#define __TINY_STRUCTS_DISPATCH__

#include <errno.h>
#include <stdbool.h>

#include <tiny/macros.h>

// An intrusive version of a dispatch table. This can be embedded as the first
// member of a struct to make it able to dispatch functions to it.
// Alternatively, if not the fist member, the ofsetof macro can be used to
// get the members of a struct.

struct tiny_dispatch {
	// Pointer to the dispatch table containing functions we can invoke
	const void *dispatch;
};

#define TINY_DISPATCH_CAST(ptr) ((struct tiny_dispatch*)(ptr))
#define TINY_DISPATCH_EMPTY() (struct tiny_dispatch) { NULL }
#define TINY_DISPATCH_INIT1(_dspch)		\
	(struct tiny_dispatch) { (_dspch) }

// Variadic constructor
#define __TINY_DISPATCH_GET_MACRO(_0, _1, MACRO, ...) MACRO
#define TINY_DISPATCH(...)												\
	__TINY_DISPATCH_GET_MACRO(_0 __VA_OPT__(, )##__VA_ARGS__,			\
							  TINY_DISPATCH_INIT1, TINY_DISPATCH_EMPTY) \
	(__VA_ARGS__)

#define _TINY_DISPATCH_TABLE(ptr, type)	\
	((const type*)(TINY_DISPATCH_CAST(ptr)->dispatch))

#define tiny_dispatch_unsafe(ptr, type, method, ...) \
	_TINY_DISPATCH_TABLE(ptr, type)->method(ptr, ##__VA_ARGS__);

static inline int tiny_dispatch_check(struct tiny_dispatch *ptr)
{
	return TINY_LIKELY(ptr) ? TINY_LIKELY(ptr->dispatch) ? 0 : -ESTALE : -EINVAL;
}

#define tiny_dispatch(ptr, type, method, ...)							\
	({																	\
		int __r = tiny_dispatch_check(TINY_DISPATCH_CAST(ptr));			\
		if (!__r) {														\
			if (TINY_LIKELY(_TINY_DISPATCH_TABLE(ptr, type)->method)) {	\
				__r = tiny_dispatch_unsafe(ptr, type, method, ##__VA_ARGS__); \
			} else {													\
				__r = -ENOSYS;											\
			}															\
		}																\
		__r;															\
	})


#endif
