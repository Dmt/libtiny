// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_VALUE__
#define __TINY_VALUE__

#include <tiny/value/type.h>
#include <tiny/value/fundamentals.h>
#include <tiny/value/array.h>
#include <tiny/value/object.h>

#endif
