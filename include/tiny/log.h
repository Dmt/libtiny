// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_LOG__
#define __TINY_LOG__

#include <tiny/interface/log.h>

#define tiny_log_error(logger, ...)                                            \
	tiny_log(logger, TINY_LOG_LEVEL_ERROR, __VA_ARGS__)
#define tiny_log_warning(logger, ...)                                          \
	tiny_log(logger, TINY_LOG_LEVEL_WARNING, __VA_ARGS__)
#define tiny_log_info(logger, ...)                                             \
	tiny_log(logger, TINY_LOG_LEVEL_INFO, __VA_ARGS__)
#define tiny_log_debug(logger, ...)                                            \
	tiny_log(logger, TINY_LOG_LEVEL_DEBUG, __VA_ARGS__)

#endif
