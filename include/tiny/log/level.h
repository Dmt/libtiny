// SPDX-FileCopyrightText: 2024 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_LOG_LEVEL__
#define __TINY_LOG_LEVEL__

#include <tiny/macros.h>

#include <errno.h>

// Unlike other logging impmenetations, we use a bitmask for the logging levels
// The advantage with this is that now we can do targetted enabling of specific
// log levels e.g. we want only the warnings from a specific file
enum tiny_log_level {
	TINY_LOG_LEVEL_NONE = 0,
	TINY_LOG_LEVEL_ERROR = TINY_BIT(0),
	TINY_LOG_LEVEL_WARNING = TINY_BIT(1),
	TINY_LOG_LEVEL_INFO = TINY_BIT(2),
	TINY_LOG_LEVEL_DEBUG = TINY_BIT(3),
};

#define TINY_LOG_LEVEL_MASK                                                    \
	(TINY_LOG_LEVEL_ERROR | TINY_LOG_LEVEL_WARNING | TINY_LOG_LEVEL_INFO | \
	 TINY_LOG_LEVEL_DEBUG)
#define TINY_LOG_LEVEL_DEFAULT					\
	(TINY_LOG_LEVEL_ERROR | TINY_LOG_LEVEL_WARNING | TINY_LOG_LEVEL_INFO)

static inline int tiny_log_parse_level(char c)
{
	switch (c) {
	case 'e':
	case 'E':
		return TINY_LOG_LEVEL_ERROR;
	case 'w':
	case 'W':
		return TINY_LOG_LEVEL_WARNING;
	case 'i':
	case 'I':
		return TINY_LOG_LEVEL_INFO;
	case 'd':
	case 'D':
		return TINY_LOG_LEVEL_DEBUG;
	case 'a':
	case 'A': // all
	case 'm':
	case 'M': // mask
		return TINY_LOG_LEVEL_MASK;
	default:
		return -EINVAL;
	};
}

#endif
