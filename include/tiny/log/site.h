// SPDX-FileCopyrightText: 2024 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_LOG_SITE__
#define __TINY_LOG_SITE__

#include <tiny/log/level.h>
#include <tiny/data-structs/list.h>

// A log site contains all the information regarding a specific logging event
// This is embedded as part of a logging macro and provied a hook to each
// logging definition.

// To access this hook from anywhere in the code, we do a trick: we place all
// these structs paced under the '_tiny_debug' section of the elf file. The
// complier knows about this trick and will make 2 variables named
// '__start__<section_name>' and __end__<section_name>. These can be referenced
// by defining these variables as 'extern'. Because of the packing, we can now
// iterate the entire section with a simple for loop.

// This gies us the ability to iterate though all debug messages, looking for
// specific features we are interrested in and enable/disable them as needed,
// which provieds very fine grained debugging

// The struct containing information for a given logger call site
struct tiny_log_site {
	struct tiny_list_node node;
	const enum tiny_log_level lvl;
	const char *category;
	const char *fmt;
	const char *file;
	const char *func;
	const unsigned int line;
	bool enabled : 1;
	bool sigtrap : 1;
};

// redefine this to enable a log category
#define TINY_LOG_DEFAULT_CATEGORY NULL

#define TINY_LOG_SITE_DEFINE(level, format)								\
	static struct tiny_log_site _info TINY_SECTION("_tiny_debug") = {	\
	    .node = TINY_LIST(_info.node),									\
	    .lvl = (level),													\
	    .category = TINY_LOG_DEFAULT_CATEGORY,							\
	    .fmt = (format),												\
	    .file = TINY_FILE_NAME,											\
	    .func = TINY_FUNC_NAME,											\
	    .line = TINY_LINE_NUM,											\
	    .enabled = false,												\
		.sigtrap = false,													\
	}


#endif
