// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_FACTORY__
#define __TINY_FACTORY__

#include <tiny/type.h>
#include <tiny/interface/context.h>

// the signature for a factory function
typedef int(tiny_factory_func_t)(struct tiny_context *ctx,
								 struct tiny_value_map *params,
								 struct tiny_clean_ptr *res);

// the actual factory struct
#define TINY_FACTORY(type, ctor) (								\
		(struct tiny_factory){TINY_TYPE(type), (ctor)})

struct tiny_factory {
	struct tiny_type type;
	tiny_factory_func_t *ctor;
};

#endif
