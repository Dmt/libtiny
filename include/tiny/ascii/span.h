// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_SIZED_STRING__
#define __TINY_SIZED_STRING__

#include <stdarg.h>
#include <stdio.h>

#include <tiny/ascii/flags.h>
#include <tiny/ascii/str.h>
#include <tiny/structs/span.h>

#define TINY_STR_SPAN(ptr) \
	TINY_SPAN_INIT2((void*)ptr, tiny_strlen(ptr))

#define TINY_SPAN_CHAR(ptr, pos) \
	*(TINY_PTR_ADD(tiny_span_start(ptr), (pos), char))

static inline char tiny_span_char(const struct tiny_span *ptr, size_t pos)
{
	return TINY_LIKELY(ptr && ptr->len > pos)
		   ? TINY_SPAN_CHAR(ptr, pos)
		   : '\0';
}

static inline char tiny_span_pop_char(struct tiny_span *ptr)
{
	struct tiny_span p = tiny_span_pop_front(ptr, 1);
	return tiny_span_char(&p, 0);
}

static inline bool tiny_span_streq(struct tiny_span *ptr,
					const char *str)
{
	return tiny_strneq(tiny_span_start(ptr), str,
			   tiny_span_len(ptr, char));
}

static inline int tiny_span_strcmp(const struct tiny_span *l,
								   const struct tiny_span *r)
{
	char lc, rc;
	size_t i, max = TINY_MAX(l->len, r->len);
	for (i = 0; i < max; i++) {
		lc = tiny_span_char(l, i);
		rc = tiny_span_char(r, i);
		if (lc != rc) {
			return lc - rc;
		}
	}
	return 0;
}

static inline int tiny_span_munch_char(struct tiny_span *ptr, char c)
{
	if (tiny_span_valid(ptr) && ptr->len && TINY_SPAN_CHAR(ptr, 0) == c) {
		tiny_span_pop_front(ptr, 1);
		return 1;
	}
	return 0;
}

static inline struct tiny_span tiny_span_munch_type(struct tiny_span *ptr,
					       enum tiny_ascii_flags flags)
{
	tiny_assert_exec(ptr && ptr->len, return TINY_SPAN());
	size_t len = 0;
	while (ptr->len && (tiny_ascii_get_flags(tiny_span_char(ptr, len)) & flags))
		len++;
	return tiny_span_pop_front(ptr, len);
}

// munches any characters contained in the "chars" string from the front of the
// buffer
static inline struct tiny_span
tiny_span_pop_chars(struct tiny_span *ptr, char *chars, size_t maxlen)
{
	size_t i;
	maxlen = TINY_MIN(maxlen, tiny_span_len(ptr, uint8_t));
	for (i = 0; i < maxlen; i++) {
		char c = tiny_span_char(ptr, i);
		char *cp;
		for (cp = chars; *cp; cp++) {
			if (c == *cp)
				break;
		}
		if (*cp == '\0')
			break;
	}
	return tiny_span_pop_front(ptr, i);
}

// Looks at the buffer for a matching character. If found, all data up to the
// character is popped
static inline struct tiny_span tiny_span_pop_sep(struct tiny_span *span,
	char sep)
{
	void *s = tiny_span_start(span);
	void *e = memchr(s, sep, tiny_span_len(span, uint8_t));
	if (e) {
		// pop the valid range
		struct tiny_span front = tiny_span_pop_front(span, e - s);
		// pop the separator
		tiny_span_pop_char(span);
		return front;
	}
	return TINY_SPAN();
}

// duplicates the string represented in the span and returnes a newely allocated
// ptr
static inline char* tiny_span_dup(struct tiny_span *span)
{
	tiny_assert_exec(span, return NULL);
	return tiny_strndup(tiny_span_start(span), tiny_span_len(span, char));
}

#endif
