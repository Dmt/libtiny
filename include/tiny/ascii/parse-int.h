// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_PARSE_INT__
#define __TINY_PARSE_INT__

#include <errno.h>
#include <tiny/ascii/flags.h>
#include <tiny/macros.h>
#include <tiny/ascii/span.h>

// NOTE: You might wonder (like i did): why pass a reference to a span and not
// just a span? a) when multistep parsing fails, we have the exact position
// of the problematic string in the parent context. b) the overflow logic
// will continue to parse a number even if it exceeds the defined range. In this
// case, the function will return an ERANGE but the span will point to after the
// syntactically valid number

// parses a single character into a number, checking that it is within the
// expected numeric base
static inline int tiny_parse_numeric_char(char c, uint8_t base)
{
	int res;
	switch (c) {
	case '0' ... '9':
		res = c - '0';
		break;
	case 'A' ... 'Z':
		res =  c - 'A' + 10;
		break;
	case 'a' ... 'z':
		res = c - 'a' + 10;
		break;
	default:
		return -EINVAL;
	}
	return res < base ? res : -EINVAL;
}

static inline int tiny_parse_sign(struct tiny_span *s,  bool *neg)
{
	tiny_assert_exec(neg, return -EINVAL);
	*neg = false;
	switch (tiny_span_char(s, 0)) {
	case '-':
		*neg = true;
		[[fallthrough]];
	case '+':
		tiny_span_pop_front(s, 1);
		return 1;
	default:
		break;
	}
	return 0;
}

static inline int tiny_parse_numeric_base(struct tiny_span *s, uint8_t *base)
{
	tiny_assert_exec(s && base, return -EINVAL);

	// I am deliberately ignoring base 8. If we ever need it we can add it.
	// On the otherhand a random 0 in the front of a number can lead to
	// unexpected bugs
	if (tiny_span_char(s, 0) == '0') {
		char c = tiny_span_char(s, 1);
		if (c == 'x' || c == 'X') {
			*base = 16;
			tiny_span_pop_front(s, 2);
			return 2;
		}
	}
	*base = 10;
	return 0;
}

static inline int tiny_accumulate_u64(struct tiny_span *s,
			uint8_t base, uint64_t max, uint64_t *accum)
{
	const uint64_t maxmul = max / base;
	const uint64_t remainder = max % base;
	bool overflow = false;
	uint32_t cnt = 0;
	int num;

	// base 36 would be 0-9 + A-Z
	tiny_assert_exec(s && accum && base > 1 && base < 37, return -EINVAL);
	while(s->len > cnt) {
		num = tiny_parse_numeric_char(tiny_span_char(s, cnt), base);
		if (num < 0)
			break;
		// At this point we know we have a valid digit to parse, so increase
		// the count
		cnt++;
		// overflow detection. since we shift by base, we know that
		// if we ever exceed max / base, the next digit after will
		// overflow. Also, if we are at the max, we can take one more
		// digit, up to the remainder.
		if (overflow || *accum > maxmul ||
		    (*accum == maxmul && (uint64_t)num > remainder)) {
			overflow = true;
			continue;
		}
		// Shift current digits by base and add the new digit
		*accum *= base;
		*accum += num;
	}
	// advance the span
	tiny_span_pop_front(s, cnt);
	// If we overflowed, reflect that
	if (overflow) {
		*accum = max;
		return -ERANGE;
	}
	return cnt;
}

static inline int tiny_parse_u64(struct tiny_span *s,
		 uint8_t base, uint64_t max, uint64_t *ret)
{
	uint64_t acc = 0;
	int res;
	res = tiny_accumulate_u64(s, base, max, &acc);
	// Return the value and the number of characters we ate
	if (ret)
		*ret = acc;
	return res;
}

static inline
int tiny_parse_u32(struct tiny_span *s, uint8_t base, uint32_t max, uint32_t *res)
{
	uint64_t tmp;
	int ret = tiny_parse_u64(s, base, max, &tmp);
	if (ret < 0)
		return ret;
	*res = tmp;
	return ret;
}

static inline
int tiny_parse_i64(struct tiny_span *s, uint8_t base, int64_t *res)
{
	bool neg;
	uint64_t acc;
	int ret;

	tiny_assert_exec(s && res, return -EINVAL);
	// first handle any prefix
	ret = tiny_parse_sign(s, &neg);
	tiny_assert_exec(ret >= 0, return ret);
	// then parse the number.
	ret = tiny_parse_u64(s, base, neg ? 0x8000000000000000 : INT64_MAX, &acc);
	// convert to the actual signed result
	// if the accumulator overflowed, it will reflect here
	*res = neg ? -((int64_t)acc) : (int64_t)acc;
	return ret;
}

#endif
