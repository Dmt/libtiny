// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_ASCII__
#define __TINY_ASCII__

#include <stdbool.h>

#include <tiny/macros.h>

enum tiny_ascii_flags {
	TINY_ASCII_INVALID = 0,
	TINY_ASCII_VALID = TINY_BIT(0),
	TINY_ASCII_UPPER = TINY_BIT(1),
	TINY_ASCII_LOWER = TINY_BIT(2),
	TINY_ASCII_NUMBER = TINY_BIT(3),
	TINY_ASCII_SPACE = TINY_BIT(4),
	TINY_ASCII_ALPHA = TINY_ASCII_UPPER | TINY_ASCII_LOWER,
	TINY_ASCII_ALPHANUM = TINY_ASCII_ALPHA | TINY_ASCII_NUMBER,
};

static inline enum tiny_ascii_flags tiny_ascii_get_flags(char c)
{
	switch (c) {
	case '\t':
	case '\n':
	case '\v':
	case '\f':
	case '\r':
	case ' ':
		return TINY_ASCII_VALID | TINY_ASCII_SPACE;
	case '0' ... '9':
		return TINY_ASCII_VALID | TINY_ASCII_NUMBER;
	case 'A' ... 'Z':
		return TINY_ASCII_VALID | TINY_ASCII_UPPER;
	case 'a' ... 'z':
		return TINY_ASCII_VALID | TINY_ASCII_LOWER;
	default:
		break;
	}
	if (c & 0x80)
		return TINY_ASCII_INVALID;
	return TINY_ASCII_VALID;
}

static inline bool tiny_ascii_is_valid(char c)
{
	// MSB needs to be unset
	return tiny_ascii_get_flags(c) & TINY_ASCII_VALID;
}

static inline bool tiny_ascii_is_lower(char c)
{
	return tiny_ascii_get_flags(c) & TINY_ASCII_LOWER;
}

static inline bool tiny_ascii_is_upper(char c)
{
	return tiny_ascii_get_flags(c) & TINY_ASCII_UPPER;
}

static inline bool tiny_ascii_is_number(char c)
{
	return tiny_ascii_get_flags(c) & TINY_ASCII_NUMBER;
}

static inline bool tiny_ascii_is_alphanum(char c)
{
	return tiny_ascii_get_flags(c) & TINY_ASCII_ALPHANUM;
}

static inline bool tiny_ascii_is_space(char c)
{
	return tiny_ascii_get_flags(c) & TINY_ASCII_SPACE;
}

static inline char tiny_ascii_to_lower(char c)
{
	return tiny_ascii_is_upper(c) ? 'a' + (c - 'A') : c;
}

static inline char tiny_ascii_to_upper(char c)
{
	return tiny_ascii_is_lower(c) ? 'A' + (c - 'a') : c;
}

#endif
