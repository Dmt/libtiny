// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_STR__
#define __TINY_STR__

#include <stdbool.h>
#include <string.h>

#include <tiny/alloc.h>
#include <tiny/macros.h>

// str-like functions for c strings

static inline size_t tiny_strlen(const char *ptr)
{
	return TINY_LIKELY(ptr) ? strlen(ptr) : 0;
}

static inline bool tiny_streq(const char *l, const char *r)
{
	return TINY_LIKELY(l && r) ? strcmp(l, r) == 0 : l == r;
}

static inline bool tiny_strneq(const char *l, const char *r, size_t len)
{
	return TINY_LIKELY(l && r) ? strncmp(l, r, len) == 0 : l == r;
}

static inline void tiny_strncpy(char *dest, const char *src, size_t num)
{
	if (TINY_LIKELY(dest && src && num)) {
		strncpy(dest, src, num);
		dest[num - 1] = '\0';
	}
}

static inline char *tiny_strdup(const char *str)
{
	if (TINY_UNLIKELY(!str))
		return NULL;
	return strdup(str);
}

static inline char *tiny_strndup(const char *str, size_t len)
{
	if (TINY_UNLIKELY(!str))
		return NULL;
	// +1 for the nul char
	len = TINY_MIN(tiny_strlen(str), len) + 1;
	char *dest = malloc(len);
	tiny_strncpy(dest, str, len);
	return dest;
}

static inline bool tiny_startswith(const char *str, const char *prefix)
{
	size_t lstr, lpfx;
	if (TINY_UNLIKELY(!str || !prefix))
		return false;
	lstr = strlen(str);
	lpfx = strlen(prefix);
	if (lstr < lpfx)
		return false;
	return strncmp(str, prefix, lpfx) == 0;
}

static inline bool tiny_endswith(const char *str, const char *suffix)
{
	size_t lstr, lsfx;
	if (TINY_UNLIKELY(!str || !suffix))
		return false;
	lstr = strlen(str);
	lsfx = strlen(suffix);
	if (lstr < lsfx)
		return false;
	return strncmp(str + lstr - lsfx, suffix, lsfx) == 0;
}

static inline size_t tiny_strvlen(const char **strv)
{
	size_t no = 0;
	tiny_assert_exec(strv, return 0);
	while (*strv) {
		no++;
		strv++;
	}
	return no;
}

static inline char *tiny_strchr(const char *s, char c)
{
	return s ? strchr(s, c) : NULL;
}

// check if string matches a pattern that may contain wildcards
static inline bool tiny_strmatch(const char *str, const char *pattern)
{
	tiny_assert_exec(pattern, return !str);
	// If string is not set we only accept if pattern is the asterisk
	if (TINY_UNLIKELY(!str))
		return pattern[0] == '*' && pattern[1] == '\0';

	const char *substr = NULL;
	const char *afterstar = NULL;
	while (*str) {
		switch (*pattern) {
		case '?':
			str++;
			pattern++;
			break;
		case '*':
			// If the next char after the star is nul, then we match
			// anything
			if (*pattern++ == '\0')
				return true;
			// Store the position after the star and the current
			// string position
			substr = str;
			afterstar = pattern;
			break;
		default:
			if (*str == *pattern) {
				str++;
				pattern++;
			} else if (!afterstar) {
				// If there were no stars, we have failed to
				// match
				return false;
			} else {
				// We have a star and the following substring
				// doesn't match Advance the string by one and
				// start over with matching after star
				str = substr++;
				pattern = afterstar;
			}
			break;
		}
	}
	// Munch any trailing stars
	while (*pattern == '*')
		++pattern;
	// The pattern needs to be exhausted at this point
	return !*pattern;
}

#endif
