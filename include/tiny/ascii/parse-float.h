// SPDX-FileCopyrightText: 2024 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_PARSE_FLOAT__
#define __TINY_PARSE_FLOAT__

#include <tiny/ascii/parse-int.h>

// This is not meant to be the fastest, just comprehensable
static inline int tiny_parse_double(struct tiny_span *s, double *ret)
{
    bool neg = false;
    int res;
    int p = 0;
    uint64_t sig = 0;
    int16_t exp = 0;

    tiny_assert_exec(s && ret, return -EINVAL);
    // chomp any sign off the start
    p = res = tiny_parse_sign(s, &neg);
    tiny_assert_exec(res >= 0, return res);
    // first parse the pre decimal point
    p += res = tiny_accumulate_u64(s, 10, UINT64_MAX, &sig);
    tiny_assert_exec(res >= 0, return res);
    // do we have a decimal?
    if (tiny_span_munch_char(s, '.')) {
        // parse the decimal point
        p++;
        p += res = tiny_accumulate_u64(s, 10, UINT64_MAX, &sig);
        tiny_assert_exec(res >= 0, return res);
        tiny_assert_exec(res < 1023, return -EOVERFLOW);
        exp = -res;
    }
    // do we have an exponent?
    if (tiny_span_char(s, 0) == 'e' || tiny_span_char(s, 0) == 'E') {
        bool eneg = false;
        uint64_t v;
        tiny_span_pop_front(s, 1);
        p++;
        // first chomp any sign
        p += res = tiny_parse_sign(s, &eneg);
        tiny_assert_exec(res >= 0, return res);
        // then the actual value
        p += res = tiny_parse_u64(s, 10, 1023, &v);
        tiny_assert_exec(res >= 0, return res);
        exp += eneg ? -v : v;
        // choosing to just fail here rather then allow special numbers
        tiny_assert_exec(exp > -1022 && exp < 1023, return -EOVERFLOW);
    }
    // now build the number. Internally, floating point numbers use
    // base 2 scientific notation. Also, not all archs use IEEE 754.
    // So we manually build the number to avoid these pitfalls
    *ret = neg ? sig * -1 : sig;
    while (exp > 0) {
        *ret *= 10;
        exp--;
    }
    while (exp < 0) {
        *ret *= 0.1;
        exp++;
    }
    return p;
}

#endif
