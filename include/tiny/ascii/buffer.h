// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_ASCII_BUFFER__
#define __TINY_ASCII_BUFFER__

#include <stdarg.h>
#include <stdio.h>
#include <errno.h>

#include <tiny/alloc.h>
#include <tiny/ascii/span.h>
#include <tiny/structs/buffer.h>

// string extensions for tiny_buffer

#define TINY_STR_BUFFER(str) \
	TINY_BUFFER(str, tiny_strlen(str))
#define TINY_STR_BUFFER_ALLOC(size) \
	TINY_BUFFER_EMPTY3(tiny_malloc0(size), (size) + 1, free)

static inline char tiny_buffer_char(struct tiny_buffer *ptr, size_t pos)
{
	return TINY_LIKELY(ptr && ptr->len > pos)
		   ? *(TINY_PTR_ADD(tiny_buffer_start(ptr), pos, char))
		   : '\0';
}

static inline bool tiny_buffer_set_char(struct tiny_buffer *ptr,
					   size_t pos, char c)
{
	if (TINY_LIKELY(ptr && ptr->len > pos)) {
		*(TINY_PTR_ADD(tiny_buffer_start(ptr), pos, char)) = c;
		return true;
	}
	return false;
}

static inline char tiny_buffer_pop_char(struct tiny_buffer *ptr)
{
	struct tiny_span s = tiny_buffer_pop_start(ptr, 1);
	return tiny_span_char(&s, 0);
}

// Looks at the buffer for a matching character. If found, all data up to the
// character is popped
static inline struct tiny_span tiny_buffer_pop_sep(struct tiny_buffer *b,
	char sep)
{
	void *s = tiny_buffer_start(b);
	void *e = memchr(s, sep, tiny_buffer_len(b));
	if (e) {
		// pop the valid range
		struct tiny_span span = tiny_buffer_pop_start(b, e - s);
		// pop the separator
		tiny_buffer_trim_start(b, 1);
		return span;
	}
	return TINY_SPAN();
}

static inline int tiny_buffer_starts_with(struct tiny_buffer *b,
	const char *pfx)
{
	tiny_assert_exec(b && pfx, return -EINVAL);
	size_t pfxlen = strlen(pfx);
	size_t blen = tiny_buffer_len(b);
	if (blen < pfxlen)
		return 0;
	return strncmp(b->mem.ptr, pfx, pfxlen) == 0;
}

// shifts all valid data to the start of the region
static inline void
tiny_buffer_shift(struct tiny_buffer *ptr)
{
	if (ptr && ptr->offset) {
		memmove(tiny_clean_ptr_get(&ptr->mem), tiny_buffer_start(ptr), ptr->len);
		ptr->offset = 0;
	}
}
/*
 * note that you can get the size needed to print to the buffer by setting the
 * ptr to NULL
 */
static inline size_t tiny_buffer_append_char(struct tiny_buffer *ptr,
						char c)
{
	if (tiny_buffer_padding_len(ptr) > 0) {
		*((char *)tiny_buffer_end(ptr)) = c;
		tiny_buffer_extend_end(ptr, 1);
	}
	return 1;
}

static inline size_t tiny_buffer_append_printf_va(struct tiny_buffer *ptr,
												  const char *fmt, va_list args)
{
	int ret = 0;
	tiny_assert_exec(fmt, return 0);
	ret = vsnprintf(tiny_buffer_padding(ptr), tiny_buffer_padding_len(ptr),
			fmt, args);
	if (TINY_LIKELY(ret >= 0))
		tiny_buffer_extend_end(ptr, ret);
	return ret;
}

static inline size_t tiny_buffer_append_printf(struct tiny_buffer *ptr,
						  const char *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	size_t res = tiny_buffer_append_printf_va(ptr, fmt, args);
	va_end(args);
	return res;
}

static inline struct tiny_buffer tiny_buffer_new_printf_len(size_t len,
	const char *fmt, ...)
{
	va_list args;
	// allocate the buffer
	struct tiny_buffer buf = tiny_buffer_new(len);
	// now, format the text
	va_start(args, fmt);
	tiny_buffer_append_printf_va(&buf, fmt, args);
	va_end(args);
	return buf;
}

static inline struct tiny_buffer tiny_buffer_new_printf(const char *fmt, ...)
{
	va_list args;
	size_t len;
	// first try to calculate the needed data for formatting the buffer
	va_start(args, fmt);
	len = tiny_buffer_append_printf_va(NULL, fmt, args) + 1;
	va_end(args);
	// allocate the buffer
	struct tiny_buffer buf = tiny_buffer_new(len);
	// now, format the text
	va_start(args, fmt);
	tiny_buffer_append_printf_va(&buf, fmt, args);
	va_end(args);
	return buf;
}

#endif
