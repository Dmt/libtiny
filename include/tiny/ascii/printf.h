// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_PRINTF__
#define __TINY_PRINTF__

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

static inline char *tiny_asprintf(const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	// one more character for the nul terminator
	int len = vsnprintf(NULL, 0, fmt, args) + 1;
	va_end(args);
	if (len <= 0)
		return NULL;

	char *p = malloc(len);
	va_start(args, fmt);
	if (vsnprintf(p, len, fmt, args) <= 0) {
		free(p);
		return NULL;
	}
	va_end(args);
	return p;
}

#endif
