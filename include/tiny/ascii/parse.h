// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#ifndef __TINY_PARSE__
#define __TINY_PARSE__

#include <tiny/ascii/parse-int.h>
#include <tiny/ascii/str.h>

// Some fundamental parsing functions

typedef int(tiny_parse_flag_t)(char c);

static inline int tiny_parse_flags(char *s, uint32_t *flags,
				   tiny_parse_flag_t *parsefunc)
{
	tiny_assert_exec(s && flags && parsefunc, return -EINVAL);

	char *start = s;
	uint32_t f = 0;
	while (*s) {
		int res = parsefunc(*s);
		if (res < 0)
			return res;
		if (f & res)
			return -EINVAL;
		f |= res;
		s++;
	}
	*flags = f;
	return s - start;
}

static inline int tiny_parse_range(char *s, uint8_t base, uint32_t max,
				   uint32_t *lstart, uint32_t *lend)
{
	char *start = s;
	char *end = tiny_strchr(s, '-');
	if (end)
		*end++ = '\0';

	struct tiny_span span = TINY_SPAN(start, end - start);
	int r = tiny_parse_u32(&span, base, max, lstart);
	int r2 = 0;
	if (r < 0)
		return r;
	if (end) {
		span = TINY_STR_SPAN(end);
		r2 = tiny_parse_u32(&span, base, max, lend);
		if (r2 < 0)
			return r2;
		if (*lstart > *lend)
			return -EINVAL;
	} else {
		*lend = *lstart;
	}
	return r + r2;
}

char *tiny_munch_type(char *s, enum tiny_ascii_flags flags)
{
	while (*s && tiny_ascii_get_flags(*s) & flags)
		s++;
	return s;
}

#endif
