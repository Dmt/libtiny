// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#include <tiny/lib/arg.h>
#include <tiny/lib/log.h>
#include <tiny/impl/log.h>

void *on_cmd(void *data)
{
	tiny_info("got command");
	return data;
}

int on_app_option(const struct tiny_arg_option *option, void *data, const char *arg);

static const struct tiny_arg_option app_opts[] = {
    {'n', "nameaaa-is-longsh", "NAME", "An optionalss with a name"},
    {'a', NULL, NULL, "A short argument"},
    {'\0', "longopt", NULL, "A long optional argument"},
    {'f', NULL, NULL, "failing option"},
    TINY_ARG_OPTION_TERM,
};

static const struct tiny_arg_verb app_verbs[] = {
    {"lookup", NULL, NULL, NULL, NULL, NULL, "Run a lookup", NULL},
    {"cmd", on_cmd, NULL, NULL, NULL, NULL, "run a command", NULL},
    TINY_ARG_VERB_TERM,
};

static const struct tiny_arg_config appInfo = {.description = "A demo parser",
					       .parse_option = on_app_option,
					       .verbs = app_verbs,
					       .opts = app_opts};

int on_app_option(const struct tiny_arg_option *option, void *data, const char *arg)
{
	TINY_UNUSED(data);
	tiny_debug("Got option '%c'", option);
	switch (option->c) {
	case '\0':
		tiny_info("A long exclusive argument '%s'", option->long_name);
		break;
	case 'n':
		tiny_info("Name arg is %s", arg);
		break;
	case 'f':
		tiny_error("triggered failure");
		return -EINVAL;
	case 's':
		tiny_info("Short arg");
		break;
	default:
		break;
	}
	return 0;
}

int main(int argc, const char *argv[])
{
	{
		struct tiny_log_filter f = TINY_LOG_FILTER(TINY_LOG_LEVEL_MASK);
		tiny_lib_log_filter(f);
		tiny_lib_log_install(tiny_impl_log_init(TINY_FD(STDOUT_FILENO)));
	}


	int ret = tiny_arg_parse(argc, argv, &appInfo, NULL);
	if (ret < 0)
		tiny_error("Failed to parse with error %d", ret);
	else
		tiny_info("Parsing done, leftover: %d", ret);
	return ret;
}
