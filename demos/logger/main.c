// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#include <tiny/lib/log.h>
#include <tiny/impl/log.h>

#include <tiny/ascii/parse.h>

#undef TINY_LOG_DEFAULT_CATEGORY
#define TINY_LOG_DEFAULT_CATEGORY "DEMO"

int parse_filter(int argc, char *argv[], struct tiny_log_filter *f)
{
	if (argc == 0)
		return 0;
	if (argc % 2 != 0)
		return -EINVAL;

	int i;
	for (i = 0; i < argc; i += 2) {
		char *k = argv[i];
		char *v = argv[i + 1];
		if (tiny_streq(k, "file")) {
			f->file = v;
		} else if (tiny_streq(k, "func")) {
			f->func = v;
		} else if (tiny_streq(k, "fmt")) {
			f->fmt = v;
		} else if (tiny_streq(k, "category")) {
			f->category = v;
		} else if (tiny_streq(k, "line")) {
			int res = tiny_parse_range(v, 10, UINT_MAX, &f->lstart,
						   &f->lend);
			if (res < 0)
				return res;
		} else if (tiny_streq(k, "lvl")) {
			int res = tiny_parse_flags(v, &f->lvl, tiny_log_parse_level);
			if (res < 0)
				return res;
		} else {
			tiny_error("Unknown filter param '%s'", k);
			return -EINVAL;
		}
	}
	return 1;
}

static void afunc()
{
	tiny_debug("A debug message");
	tiny_info("An info message");
	tiny_warning("A warning message");
	tiny_error("An error message");
}

int main(int argc, char *argv[])
{
	tiny_lib_log_install(tiny_impl_log_init(TINY_FD(STDOUT_FILENO)));

	struct tiny_log_filter f = TINY_LOG_FILTER(TINY_LOG_LEVEL_MASK);
	int res = parse_filter(argc - 1, &argv[1], &f);
	switch (res) {
	case 0:
	case 1:
		tiny_lib_log_filter(f);
		break;
	default:
		tiny_error("Failed to parse log filter with %d", res);
		return res;
	}

	tiny_debug("A debug message");
	tiny_info("An info message. We have %d args", argc);
	tiny_warning("A warning message");
	tiny_error("An error message");
	afunc();
	return 0;
}
