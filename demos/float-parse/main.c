// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#include <tiny/ascii/parse-float.h>
#include <stdio.h>

int main(int argc, const char *argv[])
{
	if (argc != 2) {
		printf("need a float string\n");
		return -EINVAL;
	}

	double d1 = 0.0, d2 = 0.0;
	struct tiny_span s = TINY_STR_SPAN((char*)argv[1]);
	int ourres = tiny_parse_double(&s, &d1);
	d2 = strtod(argv[1], NULL);

	printf("tiny Parsed: %lf(%d)\n", d1, ourres);
	printf("Std  Parsed: %lf(%d)\n", d2, errno);
	return 0;
}
