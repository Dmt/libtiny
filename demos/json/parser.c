// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#include <tiny/lib/log.h>
#include <tiny/impl/log.h>
#include <tiny/json/parser.h>

int print_fundamental(struct tiny_json_token *t)
{
	int res = -EINVAL;
	union {
		bool b;
		struct tiny_span ptr;
		int64_t i;
		double d;
	} u;
	if (tiny_json_is_null(t)) {
		tiny_info("Null");
		res = 0;
	} else if (!(res = tiny_json_get_string(t, &u.ptr))) {
		tiny_info("String: %.*s", u.ptr.len, u.ptr.ptr);
	} else if (!(res = tiny_json_get_bool(t, &u.b))) {
		tiny_info("Bool: %s", u.b ? "true" : "false");
	} else if (!(res = tiny_json_get_int(t, &u.i))) {
		tiny_info("Int: %ld", u.i);
	} else if (!(res = tiny_json_get_double(t, &u.d))) {
		tiny_info("Double: %f", u.d);
	}
	return res;
}

int print_variant(struct tiny_json_token *array);

int print_object(struct tiny_json_token *array)
{
	int res = 0;
	struct tiny_json_token *k, *v;
	tiny_assert_exec(tiny_json_is_object(array), return 0);
	tiny_info("New object %p:", array);
	tiny_json_object_foreach(k, v, array)
	{
		struct tiny_span ptr;
		res = tiny_json_get_string(k, &ptr);
		if (res)
			return res;

		tiny_info("Key: %.*s", ptr.len, ptr.ptr);

		if ((res = print_variant(v)))
			return res;
		tiny_info("-----");
	}
	if (!res)
		tiny_info("End object");
	return res;
}

int print_array(struct tiny_json_token *array)
{
	int res = 0;
	struct tiny_json_token *t;
	tiny_assert_exec(tiny_json_is_array(array), return 0);
	tiny_info("New array %p:", array);
	tiny_json_array_foreach(t, array) {
		if ((res = print_variant(t)))
			return res;
	}
	if (!res)
		tiny_info("End array");
	return res;
}

int print_variant(struct tiny_json_token *v)
{
	int res = -EINVAL;
	if (tiny_json_is_array(v))
		res = print_array(v);
	else if (tiny_json_is_object(v))
		res = print_object(v);
	else
		res = print_fundamental(v);
	return res;
}

int main(int argc, char *argv[])
{
	tiny_lib_log_filter(TINY_LOG_FILTER(TINY_LOG_LEVEL_MASK));
	tiny_lib_log_install(tiny_impl_log_init(TINY_FD(STDOUT_FILENO)));

	if (argc != 2) {
		tiny_error("Expected 1 argument containing a json string");
		return -EINVAL;
	}

	struct tiny_json_token tokens[256] = {0};
	struct tiny_json_parser p =
	    TINY_JSON_PARSER(TINY_STR_SPAN(argv[1]), tokens);

	int res = tiny_json_parse(&p);
	if (res < 0) {
		tiny_error("Failed to parse json with error %d. pos: '%s'",
			       res, tiny_span_start(&p.buffer));
		return res;
	}
	tiny_info("Parsing complete. Tokens: %u", p.pos);

	uint32_t i;
	for (i = 0; i < p.pos; i++) {
		struct tiny_span s = p.tokens[i].data;
		tiny_info("Token: %.*s", s.len, s.ptr);
	}
	// Lets try crawling the token tree
	res = print_variant(&p.tokens[0]);
	if (res)
		tiny_error("Failed to print tree %d", res);
	return res;
}
