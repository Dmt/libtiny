// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#include <tiny/impl/log.h>
#include <tiny/json/builder.h>

struct tiny_interface *l;

int main(int, char *[])
{
	TINY_CLEAN_STRUCT(tiny_json_builder) b =
		TINY_JSON_BUILDER(TINY_STR_BUFFER_ALLOC(1024));
	{
		TINY_CLEAN_STRUCT(tiny_json_builder_parent) o;
		tiny_json_builder_enter_object(&b, &o);

		tiny_json_builder_add_key_value(&b, "aKey", 123);

		tiny_json_builder_add_key_value(&b, "anotherKey", true);

		tiny_json_builder_add_key_value(&b, "aKey", "aval");

		tiny_json_builder_add_key_value(&b, "aFloatKey", 123e-3);

		tiny_json_builder_add_str(&b, "anArray");
		{
			TINY_CLEAN_STRUCT(tiny_json_builder_parent) o;
			tiny_json_builder_enter_array(&b, &o);
			tiny_json_builder_add_bool(&b, true);
			{
				TINY_CLEAN_STRUCT(tiny_json_builder_parent) o;
				tiny_json_builder_enter_array(&b, &o);
				{
					TINY_CLEAN_STRUCT(tiny_json_builder_parent) o;
					tiny_json_builder_enter_array(&b, &o);
					{
						TINY_CLEAN_STRUCT(tiny_json_builder_parent) o;
						tiny_json_builder_enter_array(&b, &o);
						{
							TINY_CLEAN_STRUCT(tiny_json_builder_parent) o;
							tiny_json_builder_enter_array(&b, &o);
						}
					}
				}
				{
					TINY_CLEAN_STRUCT(tiny_json_builder_parent) o;
					tiny_json_builder_enter_object(&b, &o);
				}
			}
			tiny_json_builder_add_int(&b, 1);
		}
	}
	printf("Json is: '%s'\n", (char *)b.ptr.mem.ptr);

	return 0;
}
