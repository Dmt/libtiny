// SPDX-FileCopyrightText: 2024 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#include <tiny/lib/context.h>
#include <tiny/lib/log.h>
#include <tiny/interface/loop.h>

#include <tiny/impl/log.h>
#include <signal.h>

static int on_sigint(struct tiny_loop_op *op, int)
{
	struct tiny_loop *loop;
	if ((loop = tiny_loop_op_user_data(op))) {
		tiny_info("Got SIGINT");
		tiny_loop_stop(loop);
	}
	return 0;
}

int main(int argc, const char *argv[])
{
	tiny_lib_log_filter(TINY_LOG_FILTER(TINY_LOG_LEVEL_MASK));
	tiny_lib_log_install(tiny_impl_log_init(TINY_FD(STDOUT_FILENO)));
	TINY_CLEAN_STRUCT_PTR(tiny_context) *ctx = NULL;

	if (argc < 2) {
		tiny_error("Need the config file path");
		return -ENOENT;
	}
	int res = tiny_context_new(argv[1], &ctx);
	if (res) {
		tiny_error("Failed to create context with error %d", res);
		return res;
	}
	// fetch the loop object and run the context using it
	struct tiny_loop *loop = tiny_context_lookup_system(ctx, tiny_loop, res);
	if (res) {
		tiny_error("Failed to lookup loop with error %d", res);
		return res;
	}
	tiny_loop_signal(loop, SIGINT, on_sigint, TINY_CLEAN_PTR(loop));

	tiny_info("Running loop %p", loop);
	res = tiny_loop_run(loop);
	tiny_info("Out of loop with %d", res);

	return res;
}
