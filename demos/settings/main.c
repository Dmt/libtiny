// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#include <tiny/lib/arg.h>
#include <tiny/lib/log.h>
#include <tiny/value/builder.h>
#include <tiny/ascii/str.h>

#include <tiny/impl/log.h>
#include <tiny/impl/settings.h>

#include <errno.h>
#include <stdio.h>

static int on_app_option(const struct tiny_arg_option *option, void *data,
						  const char *arg);

static const struct tiny_arg_option app_opts[] = {
    {'g', "get", "key", "performs a lookup on the specified key"},
    TINY_ARG_OPTION_TERM,
};

static const struct tiny_arg_config appInfo = {
	.description = "Unified settings",
	.parse_option = on_app_option,
	.verbs = NULL,
	.opts = app_opts
};

static void print_val(const char *key, const struct tiny_value *v)
{
	if (!v) {
		tiny_debug("it %p: (nul)", v);
		return;
	}
	tiny_debug("val %p: %u %u %u %u %u: ", v, v->size, v->reserved, v->type,
			   v->meta[0], v->meta[1]);
	switch(v->type) {
	case TINY_VALUE_STRING:
		tiny_info("Key: %s='%s'", key, TINY_VALUE_STR(v));
		break;
	case TINY_VALUE_BOOL: {
		bool b;
		tiny_value_get(v, &b);
		tiny_info("Key: %s=%d", key, b);
		break;
	}
	case TINY_VALUE_INT: {
		int64_t val;
		tiny_value_get(v, &val);
		tiny_info("Key: %s=%ld", key, val);
		break;
	}
	case TINY_VALUE_DOUBLE: {
		double d;
		tiny_value_get(v, &d);
		tiny_info("Key: %s=%f", key, d);
		break;
	}
	default:
		tiny_info("Key: %s", key);
		break;
	}
}
int on_app_option(const struct tiny_arg_option *option, void *data, const char *arg)
{
	struct tiny_settings *s = data;
	switch (option->c) {
	case 'g': {
		const struct tiny_value *v;
		int res = tiny_settings_lookup(s, TINY_STR_SPAN(arg), &v);
		if (res) {
			tiny_error("failed to find key '%s'", arg);
			return res;
		}
		print_val(arg, v);
	} break;
	case 'l': {

	}
	default:
		break;
	}
	return 0;
}


int main(int argc, const char *argv[])
{
	tiny_lib_log_filter(TINY_LOG_FILTER(TINY_LOG_LEVEL_MASK));
	tiny_lib_log_install(tiny_impl_log_init(TINY_FD(STDOUT_FILENO)));

	TINY_CLEAN_STRUCT(tiny_value_builder) bldr =
		TINY_VALUE_BUILDER(TINY_ARRAY(256));
	{
		TINY_CLEAN_STRUCT(tiny_value_builder_parent) p;
		tiny_value_builder_enter_object(&bldr, &p);
		tiny_value_builder_add_key_value(&bldr, "a.key", "AVAL");
		tiny_value_builder_add_key_value(&bldr, "another.key", 123);
	}

	// load the setting interface
	TINY_CLEAN_STRUCT(tiny_clean_ptr) settings = {};
	int res = tiny_impl_settings_init(tiny_value_builder_ptr(&bldr), &settings);
	if (res < 0) {
		tiny_error("Failed to load settings with error %d", res);
		return res;
	}

	// now we can parse arguments
	return tiny_arg_parse(argc, argv, &appInfo, settings.ptr);
}
