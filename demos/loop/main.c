// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#include <signal.h>

#include <tiny/ascii/buffer.h>
#include <tiny/impl/log.h>
#include <tiny/value.h>
#include <tiny/lib/log.h>
#include <tiny/lib/context.h>
#include <tiny/sys/time.h>
#include <tiny/interface/loop.h>

#include <unistd.h>

struct tiny_loop *loop = NULL;
int count = 0;

static int on_timer(struct tiny_loop_op *op, int res)
{
	tiny_info("timer event with res %d", res);
	return tiny_loop_op_arm(op);
}

static int on_sigint(struct tiny_loop_op *, int res)
{
	tiny_info("Got SIGINT with res %d", res);
	tiny_loop_stop(loop);
	return 0;
}

static int on_read(struct tiny_loop_op *op, int res)
{
	struct tiny_buffer *buf = tiny_loop_op_buffer(op);
	if (res < 0) {
		tiny_error("Got error when reading: %d", res);
		return res;
	}
	// trim the newline from the end
	tiny_buffer_trim_end(buf, 1);
	tiny_info("Got input: res:%d s:\"%.*s\"", res, tiny_buffer_len(buf), tiny_buffer_start(buf));
	if (count++ > 5)
		tiny_loop_stop(loop);
	tiny_buffer_resize(buf, 0, buf->size);
	return tiny_loop_op_arm(op);
}

static int on_write(struct tiny_loop_op *op, int res)
{
	struct tiny_buffer *buf = tiny_loop_op_buffer(op);
	tiny_info("write complete with %d, bytes left: %d", res, tiny_buffer_len(buf));
	return tiny_buffer_len(buf) ? tiny_loop_op_arm(op) : 0;
}

int main(int argc, const char *argv[])
{
	tiny_lib_log_filter(TINY_LOG_FILTER(TINY_LOG_LEVEL_MASK));
	tiny_lib_log_install(tiny_impl_log_init(TINY_FD(STDOUT_FILENO)));
	TINY_CLEAN_STRUCT_PTR(tiny_context) *ctx = NULL;

	if (argc < 2) {
		tiny_error("Need the config file path");
		return -ENOENT;
	}
	int res = tiny_context_new(argv[1], &ctx);
	if (res) {
		tiny_error("Failed to create context with error %d", res);
		return -errno;
	}
	// fetch the loop object and run the context using it
	loop = tiny_context_lookup_system(ctx, tiny_loop, res);
	if (res) {
		tiny_error("Failed to lookup loop with error %d", res);
		return res;
	}
	res = tiny_loop_signal(loop, SIGINT, on_sigint, TINY_CLEAN_PTR());
	tiny_info("signal handler returned %d", res);
	res = tiny_loop_timer(loop, TINY_TIMESTAMP_MONO(TINY_SECOND), on_timer,
					TINY_CLEAN_PTR());
	tiny_info("timer returned %d", res);

	struct tiny_buffer buf = tiny_buffer_new(1024);
	res = tiny_loop_read(loop, TINY_FD(STDIN_FILENO), &buf, on_read, TINY_CLEAN_PTR());
	tiny_info("read returned %d", res);
	buf = tiny_buffer_new_printf("PRINTF %d\n", 123);
	res = tiny_loop_write(loop, TINY_FD(STDOUT_FILENO), &buf, on_write, TINY_CLEAN_PTR());
	tiny_info("write returned %d", res);

	tiny_info("Running loop %p", loop);
	res = tiny_loop_run(loop);
	tiny_info("Out of loop with res %d", res);
	return 0;
}
