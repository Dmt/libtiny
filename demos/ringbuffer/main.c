// SPDX-FileCopyrightText: 2022 Dimitrios Katsaros <patcherwork@gmail.com>
//
// SPDX-License-Identifier: MIT

#include <pthread.h>
#include <sched.h>

#include <tiny/alloc.h>
#include <tiny/lib/log.h>
#include <tiny/data-structs/ring-buffer.h>

#include <tiny/impl/log.h>

#define RBSIZE (1 << 12)
#define WRITEMAX 13u
#define READMAX 4u
bool running = true;
struct tiny_ring_buffer rb;

struct complex {
	uint64_t bv;
	bool print;
	uint8_t padding[3];
};

void *read_cb(void *p)
{
	uint32_t i;
	uint64_t counter = 0;
	struct complex *data = p;
	bool synced = true;

	while (running) {
		uint32_t pos = 0, len = 0;
		int res = tiny_ring_buffer_read_region(&rb, &pos, &len);
		len = TINY_MIN(len, READMAX);
		switch (res) {
		case TINY_RB_UNDERRUN:
			tiny_info("Underrun!");
			synced = false;
			sched_yield();
			continue;
		case TINY_RB_OVERRUN:
			uint32_t wp = rb.writepos;
			tiny_info("Overrun! distance %u %u %u",
				  tiny_ring_buffer_distance(&rb), rb.readpos,
				  wp);
			// overruns specifically we will never get the data, so
			// seek to the current write position
			tiny_ring_buffer_seek(&rb, wp);
			synced = false;
			continue;
		case 0:
			break;
		default:
			tiny_error("Error when getting read region %d", res);
		}

		// tiny_info("Got read region %u %u", pos, len);
		struct complex *ptr = &data[pos];
		for (i = 0; i < len; i++) {
			if (!synced) {
				synced = true;
				counter = ptr[i].bv;
			}
			if (counter != ptr[i].bv) {
				tiny_error("Incorrect data read! %u != %u",
					   counter, ptr[i]);
			}
			counter++;
		}
		// tiny_info("read %u samples", len);
		tiny_ring_buffer_read_advance(&rb, len);
	}

	return NULL;
};

int main()
{
	tiny_lib_log_filter(TINY_LOG_FILTER(TINY_LOG_LEVEL_MASK));
	tiny_lib_log_install(tiny_impl_log_init(TINY_FD(STDOUT_FILENO)));

	TINY_AUTO_FREE struct complex *data =
	    malloc(RBSIZE * sizeof(struct complex));
	rb = tiny_ring_buffer_init(RBSIZE);

	pthread_t read_thread;
	pthread_create(&read_thread, NULL, read_cb, data);

	bool dontOverflow = true;
	uint32_t pos, len, i;
	uint64_t counter = 0;
	while (running) {
		int res = tiny_ring_buffer_write_region(&rb, &pos, &len);
		if (res < 0) {
			tiny_error("Failed to get write region with %d", res);
			return -res;
		} else if (res == TINY_RB_WILL_OVERFLOW && dontOverflow) {
			sched_yield();
			continue;
		}
		// tiny_info("Got write region %u %u", pos, len);
		len = TINY_MIN(len, WRITEMAX);

		struct complex *ptr = &data[pos];
		for (i = 0; i < len; i++)
			ptr[i].bv = counter++;
		// tiny_info("wrote %u samples", len);
		tiny_ring_buffer_write_advance(&rb, len);
	}
	return 0;
}
